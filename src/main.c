/*
 * main.c
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <stdio.h>
#include <stdlib.h>

#include <sefht/sefht.h>

static inline
void
test_html_generation ()
{
	SH_Data * data;
	SH_Fragment * root;
	SH_Fragment * child1;
	SH_Fragment * child2;
	SH_Fragment * child3;
	SH_Text * text;

	data = SH_Data_new (NULL);

	root = SH_NodeFragment_new ("html", data, NULL);
	SH_NodeFragment_append_attr_new ((struct SH_NodeFragment *)root, "lang", "de", NULL);

	child1 = SH_NodeFragment_new ("head", data, NULL);
	SH_NodeFragment_append_child ((struct SH_NodeFragment *) root,
				      child1, NULL);

	child2 = SH_NodeFragment_new ("meta", data, NULL);
	SH_NodeFragment_append_attr_new ((struct SH_NodeFragment *)child2, "charset", "utf-8", NULL);
	SH_NodeFragment_append_child ((struct SH_NodeFragment *) child1,
				      child2, NULL);

	child2 = SH_NodeFragment_new ("title", data, NULL);
	SH_NodeFragment_append_child ((struct SH_NodeFragment *) child1,
				      child2, NULL);

	child2 = SH_NodeFragment_new ("link", data, NULL);
	SH_NodeFragment_append_attr_new ((struct SH_NodeFragment *)child2, "rel", "stylesheet", NULL);
	SH_NodeFragment_append_attr_new ((struct SH_NodeFragment *)child2, "type", "text/css", NULL);
	SH_NodeFragment_append_attr_new ((struct SH_NodeFragment *)child2, "href", "/global.css", NULL);
	SH_NodeFragment_append_child ((struct SH_NodeFragment *) child1,
				      child2, NULL);

	child2 = SH_NodeFragment_new ("link", data, NULL);
	SH_NodeFragment_append_attr_new ((struct SH_NodeFragment *)child2, "rel", "stylesheet", NULL);
	SH_NodeFragment_append_attr_new ((struct SH_NodeFragment *)child2, "type", "text/css", NULL);
	SH_NodeFragment_append_attr_new ((struct SH_NodeFragment *)child2, "href", "index.css", NULL);
	SH_NodeFragment_append_child ((struct SH_NodeFragment *) child1,
				      child2, NULL);

	child2 = SH_NodeFragment_new ("meta", data, NULL);
	SH_NodeFragment_append_attr_new ((struct SH_NodeFragment *)child2, "name", "author", NULL);
	SH_NodeFragment_append_attr_new ((struct SH_NodeFragment *)child2, "content", "Jonathan Schöbel", NULL);
	SH_NodeFragment_append_child ((struct SH_NodeFragment *) child1,
				      child2, NULL);

	child2 = SH_NodeFragment_new ("meta", data, NULL);
	SH_NodeFragment_append_attr_new ((struct SH_NodeFragment *)child2, "name", "date", NULL);
	SH_NodeFragment_append_attr_new ((struct SH_NodeFragment *)child2, "content", "2020-05-16T01:15:49+0200", NULL);
	SH_NodeFragment_append_child ((struct SH_NodeFragment *) child1,
				      child2, NULL);

	child2 = SH_NodeFragment_new ("meta", data, NULL);
	SH_NodeFragment_append_attr_new ((struct SH_NodeFragment *)child2, "name", "description", NULL);
	SH_NodeFragment_append_attr_new ((struct SH_NodeFragment *)child2, "content", "Homepage of www2.xn--schbel-yxa.info", NULL);
	SH_NodeFragment_append_child ((struct SH_NodeFragment *) child1,
				      child2, NULL);

	child2 = SH_NodeFragment_new ("meta", data, NULL);
	SH_NodeFragment_append_attr_new ((struct SH_NodeFragment *)child2, "name", "keywords", NULL);
	SH_NodeFragment_append_attr_new ((struct SH_NodeFragment *)child2, "content", "", NULL);
	SH_NodeFragment_append_child ((struct SH_NodeFragment *) child1,
				      child2, NULL);

	child2 = SH_NodeFragment_new ("meta", data, NULL);
	SH_NodeFragment_append_attr_new ((struct SH_NodeFragment *)child2, "name", "copyright", NULL);
	SH_NodeFragment_append_attr_new ((struct SH_NodeFragment *)child2, "content", "Jonathan Sch&ouml;bel 2020", NULL);
	SH_NodeFragment_append_child ((struct SH_NodeFragment *) child1,
				      child2, NULL);

	child2 = SH_NodeFragment_new ("meta", data, NULL);
	SH_NodeFragment_append_attr_new ((struct SH_NodeFragment *)child2, "name", "generator", NULL);
	SH_NodeFragment_append_attr_new ((struct SH_NodeFragment *)child2, "content", "Bluefish 2.2.7", NULL);
	SH_NodeFragment_append_child ((struct SH_NodeFragment *) child1,
				      child2, NULL);


	child1 = SH_NodeFragment_new ("body", data, NULL);
	SH_NodeFragment_append_child ((struct SH_NodeFragment *) root,
				      child1, NULL);

	child2 = SH_NodeFragment_new ("header", data, NULL);
	SH_NodeFragment_append_child ((struct SH_NodeFragment *) child1,
				      child2, NULL);

	child3 = SH_NodeFragment_new ("h1", data, NULL);
	SH_NodeFragment_append_child ((struct SH_NodeFragment *) child2,
				      child3, NULL);

	child3 = SH_NodeFragment_new ("nav", data, NULL);
	SH_NodeFragment_append_child ((struct SH_NodeFragment *) child2,
				      child3, NULL);


	child2 = SH_NodeFragment_new ("main", data, NULL);
	SH_NodeFragment_append_child ((struct SH_NodeFragment *) child1,
				      child2, NULL);

	child2 = SH_NodeFragment_new ("footer", data, NULL);
	SH_NodeFragment_append_child ((struct SH_NodeFragment *) child1,
				      child2, NULL);

	text = SH_Fragment_to_html (root, WRAP, 0, 1, INDENT_TEXT, NULL);
	SH_Text_print (text);
	printf ("\n");

	SH_Text_free (text);

	text = SH_Fragment_to_html (root, INLINE, 0, 1, INDENT_TEXT, NULL);
	SH_Text_print (text);
	printf ("\n");

	SH_Text_free (text);
	SH_Fragment_free (root);
	SH_Data_free (data);
	return;
}

int main(int argc, char **argv)
{
	page_t page;

	/* startup */
	SH_Cms * cms = SH_Cms_new (NULL);
	/* startup */

	page = SH_Cms_register_page (cms, "Startpage", NULL);

	/* shutdown */
	SH_Cms_free (cms);
	/* shutdown */

	(void) argc;
	(void) argv;
	(void) page;

	test_html_generation ();
	return 0;
}

