/*
 * validator_tag.c
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "macro.h"
#include "log.h"
#include "status.h"

#include "validator_tag.h"


#ifndef VALIDATOR_IS_DEFINED

#define NEXT_TAG(tag) (tag + 1)

struct tag_info
{
	union
	{
		struct
		{
			Tag id;
			/*@only@*/ char * name;
		} data;
		size_t next;
	};
};

#define TAG_DATA                                                       \
	/*@only@*/ struct tag_info * tags;                             \
	size_t tag_n;                                                  \
	Tag last_tag;                                                  \

#else /* VALIDATOR_IS_DEFINED */

static inline
bool
init_tags (/*@special@*/ struct SH_Validator * validator,
           /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@defines validator->tags,
	        validator->tag_n,
	        validator->last_tag@*/
	/*@modifies validator->tags@*/
	/*@modifies validator->tag_n@*/
	/*@modifies validator->last_tag@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

static inline
bool
copy_tags (/*@special@*/ struct SH_Validator * copy,
           const struct SH_Validator * validator,
           /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@defines copy->tags,
	           copy->tag_n,
	           copy->last_tag@*/
	/*@modifies copy->tags@*/
	/*@modifies copy->tag_n@*/
	/*@modifies copy->last_tag@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

static inline
void
free_tags (/*@special@*/ struct SH_Validator * validator)
	/*@modifies validator->tags@*/
	/*@releases validator->tags@*/;

static inline
size_t
get_tag_number (const struct SH_Validator * validator)
	/*@*/;

static inline
Tag
add_tag (struct SH_Validator * validator,
         const char * tag,
         /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@modifies validator->tags@*/
	/*@modifies validator->tag_n@*/
	/*@modifies validator->last_tag@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

/*@unused@*/
static inline
bool
is_tag_id (const struct SH_Validator * validator, Tag id)
	/*@*/;

static inline
bool
is_tag_name (const struct SH_Validator * validator, const char * name)
	/*@*/;

/*@unused@*/
static inline
/*@null@*/
/*@only@*/
char *
get_tag_name_by_id (const struct SH_Validator * validator, Tag id,
                    /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

static inline
Tag
get_tag_id_by_name (const struct SH_Validator * validator,
                    const char * name)
	/*@*/;

static inline
bool
remove_tag (struct SH_Validator * validator, Tag id,
            /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@modifies validator->tag_n@*/
	/*@modifies validator->tags@*/
	/*@modifies validator->last_tag@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;


#define EXECUTE_ON_ALL_TAGS_IF(ITER, CONDITION, BLOCK)                 \
do                                                                     \
{                                                                      \
	bool is_free;                                                  \
	size_t index;                                                  \
	size_t free_index;                                             \
                                                                       \
	for (index = (size_t) 1; index <= ITER##_n; index++)           \
	{                                                              \
		/* if tag is not in the list of free blocks */         \
		is_free = FALSE;                                       \
		for (free_index = ITER##s[0].next;                     \
		     free_index != 0;                                  \
		     free_index = ITER##s[free_index].next)            \
		{                                                      \
			if (index == free_index)                       \
			{                                              \
				is_free = TRUE;                        \
                                                                       \
				/*@innerbreak@*/                       \
				break;                                 \
			}                                              \
		}                                                      \
                                                                       \
		if (!is_free && CONDITION) BLOCK                       \
	}                                                              \
}                                                                      \
while (FALSE)

#define EXECUTE_ON_ALL_TAGS(BASE, BLOCK)                               \
        EXECUTE_ON_ALL_TAGS_IF (BASE, TRUE, BLOCK)


static inline
bool
init_tags (/*@special@*/ struct SH_Validator * validator,
           /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@defines validator->tags,
	           validator->tag_n,
	           validator->last_tag@*/
	/*@modifies validator->tags@*/
	/*@modifies validator->tag_n@*/
	/*@modifies validator->last_tag@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	validator->tags = malloc (sizeof (struct tag_info));
	if (validator->tags == NULL)
	{
		set_status (status, E_ALLOC, 3, "malloc failed");

		validator->tag_n = 0;
		validator->last_tag = TAG_ERR;
		return FALSE;
	}

	validator->tags[0].next = 0;
	validator->tag_n = 0;
	validator->last_tag = TAG_ERR;

	return TRUE;
}

static inline
bool
copy_tags (/*@special@*/ struct SH_Validator * copy,
           const struct SH_Validator * validator,
           /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@defines copy->tags,
	           copy->tag_n,
	           copy->last_tag@*/
	/*@modifies copy->tags@*/
	/*@modifies copy->tag_n@*/
	/*@modifies copy->last_tag@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	bool is_free;
	size_t index;
	size_t free_index;
	size_t copy_index;
	size_t tag_n;

	tag_n = get_tag_number (validator);

	/* The size calculation is save,
	 * because validator is already allocated. */
	copy->tags = malloc ((tag_n + 1) * sizeof (struct tag_info));

	if (copy->tags == NULL)
	{
		set_status (status, E_ALLOC, 5, "malloc failed");

		copy->tag_n = 0;
		copy->last_tag = TAG_ERR;
		return FALSE;
	}

	/* copy allocation info */
	copy->tags[0].next = 0;
	copy->tag_n = tag_n;
	copy->last_tag = validator->last_tag;


	/* copy data */
	copy_index = 0;
	for (index = (size_t) 1; index <= validator->tag_n; index++)
	{
		/* if tag is not in the list of free blocks */
		is_free = FALSE;
		for (free_index = validator->tags[0].next;
		     free_index != 0;
		     free_index = validator->tags[free_index].next)
		{
			if (index == free_index)
			{
				is_free = TRUE;

				/*@innerbreak@*/
				break;
			}
		}

		if (!is_free)
		{
			copy->tags[copy_index].data.id =
			              validator->tags[index].data.id;

			copy->tags[copy_index].data.name = strdup (
			              validator->tags[index].data.name);

			if (copy->tags[copy_index].data.name == NULL)
			{
				size_t index;

				set_status (status, E_ALLOC, 5,
				            "strdup failed");

				for (index = 0; index < copy_index;
				     index++)
				{
					free (copy->tags[index]
					                    .data.name);
				}

				free (copy->tags);

				return FALSE;
			}

			copy_index++;
		}
	}

	return TRUE;
}

static inline
void
free_tags (/*@special@*/ struct SH_Validator * validator)
	/*@modifies validator->tags@*/
	/*@releases validator->tags@*/
{
	EXECUTE_ON_ALL_TAGS (
	    validator->tag,
	{
		free (validator->tags[index].data.name);
	});

	free (validator->tags);
	return;
}

static inline
size_t
get_tag_number (const struct SH_Validator * validator)
	/*@*/
{
	size_t tag_n;

	tag_n = 0;

	EXECUTE_ON_ALL_TAGS (
	    validator->tag,
	{
		/* This addition is always save,
		 * because tag_n is always smaller than
		 * validator->tag_n and it is also of size_t. */
		tag_n++;
	});

	return tag_n;
}

static inline
Tag
add_tag (struct SH_Validator * validator,
         const char * tag,
         /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@modifies validator->tags@*/
	/*@modifies validator->tag_n@*/
	/*@modifies validator->last_tag@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	Tag tag_id;
	size_t index;
	bool is_new;

	/* abort on overflow:
	 * - no unused Tag or
	 * - no unused index */
	if ((validator->last_tag == TAG_MAX)
	|| ((validator->tags[0].next == 0)
	    && ((validator->tag_n >= SIZE_MAX - 1)
	        || ((validator->tag_n + 2)
	            > (SIZE_MAX / sizeof (struct tag_info))))))
	{
		set_status (status, E_DOMAIN, 2,
		            "maximum number of tags reached");
		return TAG_ERR;
	}

	if (validator->tags[0].next == 0)
	/* allocate new space */
	{
		struct tag_info * new_tags;

		/* The addition and the multiplication is save,
		 * because we have tested for this
		 * in the first condition. */
		new_tags = realloc (validator->tags,
		                    sizeof (struct tag_info)
		                    * (validator->tag_n + 2));

		if (new_tags == NULL)
		{
			set_status (status, E_ALLOC, 6,
			            "realloc failed");

/* bad code to silence splint, should never be executed. */
#ifdef S_SPLINT_S
			validator->tags = (void *) 0x12345;
#endif
			return TAG_ERR;
		}

		validator->tags = new_tags;
		index = validator->tag_n + 1;
		is_new = TRUE;
	}
	/* reuse old space */
	else
	{
		index = validator->tags[0].next;
		validator->tags[0].next = validator->tags[index].next;
		is_new = FALSE;
	}

	tag_id = NEXT_TAG (validator->last_tag);
	validator->tags[index].data.id = tag_id;
	validator->tags[index].data.name = strdup (tag);

	if (validator->tags[index].data.name == NULL)
	{
		set_status (status, E_ALLOC, 4, "strdup failed");

		/* restore free space list */
		if (!is_new)
		{
			validator->tags[index].next =                  \
			                        validator->tags[0].next;
			validator->tags[0].next = index;
		}

		return TAG_ERR;
	}

	/* commit changes */
	if (is_new)
	{
		validator->tag_n++;
	}

	validator->last_tag = tag_id;

	set_success (status);

	return tag_id;
}

/*@unused@*/
static inline
bool
is_tag_id (const struct SH_Validator * validator, Tag id)
	/*@*/
{
	EXECUTE_ON_ALL_TAGS_IF (
	    validator->tag,
	    (validator->tags[index].data.id == id),
	{
		return TRUE;
	});

	return FALSE;
}

static inline
bool
is_tag_name (const struct SH_Validator * validator, const char * name)
	/*@*/
{
	EXECUTE_ON_ALL_TAGS_IF (
	    validator->tag,
	    (strcmp (validator->tags[index].data.name, name) == 0),
	{
		return TRUE;
	});

	return FALSE;
}

/*@unused@*/
static inline
/*@null@*/
/*@only@*/
char *
get_tag_name_by_id (const struct SH_Validator * validator, Tag id,
                    /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	char * name;

	EXECUTE_ON_ALL_TAGS_IF (
	    validator->tag,
	    (validator->tags[index].data.id == id),
	{
		name = strdup (validator->tags[index].data.name);
		if (name == NULL)
		{
			set_status (status, E_ALLOC, 3,
			            "strdup failed");
			return NULL;
		}

		return name;
	});

	return NULL;
}

static inline
Tag
get_tag_id_by_name (const struct SH_Validator * validator,
                    const char * name)
	/*@*/
{
	EXECUTE_ON_ALL_TAGS_IF (
	    validator->tag,
	    (strcmp (validator->tags[index].data.name, name) == 0),
	{
		return validator->tags[index].data.id;
	});

	return TAG_ERR;
}

static inline
bool
remove_tag (struct SH_Validator * validator, Tag id,
            /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@modifies validator->tag_n@*/
	/*@modifies validator->tags@*/
	/*@modifies validator->last_tag@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	#define tags validator->tags
	#define tag_n validator->tag_n
	EXECUTE_ON_ALL_TAGS_IF (
	    tag,
	    (tags[index].data.id == id),
	{
		free (tags[index].data.name);

		if (index == tag_n)
		{
			struct tag_info * new_tags;

			do
			{
				/* find next last free blocks
				 * in the list of free blocks */
				is_free = FALSE;
				for (free_index = tags[0].next;
				     free_index != 0;
				     free_index = tags[free_index].next)
				{

				if (tags[free_index].next == (index - 1))
				{
					is_free = TRUE;
					index--;

					tags[free_index].next =
					tags[tags[free_index].next].next;

					/*@innerbreak@*/
					break;
				}
				}
			}
			while (is_free);

			if (index
			> (SIZE_MAX / sizeof (struct tag_info)))
			{
				set_status (status, E_DOMAIN, 2,
				      "overflow while calling realloc");
			}

			new_tags = realloc (tags,
			                    sizeof (struct tag_info)
			                    * index);

			if (new_tags == NULL)
			{
				set_status (status, E_ALLOC, 4,
				            "realloc failed");

/* bad code to silence splint, should never be executed. */
#ifdef S_SPLINT_S
				tags = (void *) 0x12345;
#endif
				return FALSE;
			}

			tags = new_tags;
		}
		else
		{
			tags[index].next = tags[0].next;
			tags[0].next = index;
		}

		set_success (status);
		return TRUE;
	});
	#undef tags
	#undef tag_n

	set_status (status, E_VALUE, 68, "unknown Tag id");
	return FALSE;
}


bool
SH_Validator_check_tag (struct SH_Validator * validator,
                        const char * tag)
	/*@*/
{
	return is_tag_name (validator, tag);
}

Tag
/*@alt void@*/
SH_Validator_register_tag (struct SH_Validator * validator,
                           const char * tag,
                           /*@null@*/ /*@out@*/
                           struct SH_Status * status)
	/*@modifies validator->tags@*/
	/*@modifies validator->tag_n@*/
	/*@modifies validator->last_tag@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	Tag tag_id;

	/* tag already registered */
	tag_id = get_tag_id_by_name (validator, tag);
	if (tag_id != TAG_ERR)
	{
		return tag_id;
	}

	return add_tag (validator, tag, status);
}

bool
SH_Validator_deregister_tag (struct SH_Validator * validator,
                             Tag id,
                             /*@null@*/ /*@out@*/
                             struct SH_Status * status)
	/*@modifies validator->tag_n@*/
	/*@modifies validator->tags@*/
	/*@modifies validator->last_tag@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	return remove_tag (validator, id, status);
}

#endif /* VALIDATOR_IS_DEFINED */
