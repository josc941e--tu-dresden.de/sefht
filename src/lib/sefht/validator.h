/*
 * validator.h
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef SEFHT_VALIDATOR_H
#define SEFHT_VALIDATOR_H

#if !defined (SEFHT_SEFHT_H_INSIDE) && !defined (SEFHT_COMPILATION)
#error "Only <sefht/sefht.h> can be included directly."
#endif

#include "status.h"


typedef struct SH_Validator SH_Validator;

#define __VALIDATOR_H_INSIDE__
#include "validator_tag.h"
#undef __VALIDATOR_H_INSIDE__


/*@null@*/
/*@only@*/
SH_Validator *
SH_Validator_new (/*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

/*@null@*/
/*@only@*/
SH_Validator *
SH_Validator_copy (const SH_Validator * validator,
                   /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

void
SH_Validator_free (/*@only@*/ struct SH_Validator * validator)
	/*@modifies validator@*/
	/*@releases validator@*/;


#endif /* SEFHT_VALIDATOR_H */
