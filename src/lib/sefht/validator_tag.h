/*
 * validator_tag.h
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef SEFHT_VALIDATOR_TAG_H
#define SEFHT_VALIDATOR_TAG_H

#if !defined (SEFHT_VALIDATOR_H)
#error "Please include only <sefht/validator.h>."
#endif

#include <stdbool.h>
#include <stdint.h>

#include "status.h"


typedef unsigned int Tag;
#define TAG_ERR (Tag) 0
#define TAG_MIN (Tag) 1
#define TAG_MAX (Tag) SIZE_MAX


Tag
/*@alt void@*/
SH_Validator_register_tag (SH_Validator * validator,
                           const char * tag,
                           /*@null@*/ /*@out@*/
                           struct SH_Status * status)
	/*@modifies validator@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_Validator_deregister_tag (SH_Validator * validator,
                             Tag id,
                             /*@null@*/ /*@out@*/
                             struct SH_Status * status)
	/*@modifies validator@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_Validator_check_tag (struct SH_Validator * validator,
                        const char * tag)
	/*@*/;

#endif /* SEFHT_VALIDATOR_TAG_H */
