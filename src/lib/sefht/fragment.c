/*
 * fragment.c
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "macro.h"
#include "log.h"
#include "status.h"

#include "text.h"
#include "node_fragment.h"

#include "fragment.h"


#include "fragment_class.c"

/*@null@*/
/*@only@*/
struct SH_Fragment *
SH_Fragment_copy (const struct SH_Fragment * fragment,
                  /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	return fragment->methods->copy (fragment, status);
}

void
SH_Fragment_free (/*@only@*/ struct SH_Fragment * fragment)
	/*@modifies fragment@*/
	/*@releases fragment@*/
{
	fragment->methods->free (fragment);
	return;
}

bool
SH_Fragment_is_orphan (const struct SH_Fragment * fragment)
	/*@*/
{
	return get_parent (fragment) == NULL;
}

/*@null@*/
/*@only@*/
SH_Text *
SH_Fragment_to_html (const struct SH_Fragment * fragment,
                     enum HTML_MODE mode,
                     unsigned int indent_base,
                     unsigned int indent_step,
                     const char * indent_char,
                     /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	return fragment->methods->to_html (fragment, mode,
	                                   indent_base,
	                                   indent_step,
	                                   indent_char,
	                                   status);
}
