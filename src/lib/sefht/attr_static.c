/*
 * attr_static.c
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef SEFHT_ATTR_STATIC_C
#define SEFHT_ATTR_STATIC_C

#if !defined (SEFHT_SEFHT_H_INSIDE) && !defined (SEFHT_COMPILATION)
#error "Only <sefht/sefht.h> can be included directly."
#endif

#include "attr_data.h"


static inline
bool
Attr_init (/*@special@*/ struct SH_Attr * attr,
           const char * name,
           /*@null@*/ const char * value,
           /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@defines attr->name,
	           attr->value@*/
	/*@modifies attr->name@*/
	/*@modifies attr->value@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	attr->name = strdup (name);
	if (NULL == attr->name)
	{
		set_status (status, E_ALLOC, 2, "strdup failed");
		return FALSE;
	}

	if (NULL == value)
	{
		attr->value = NULL;
	}
	else
	{
		attr->value = strdup (value);
		if (NULL == attr->value)
		{
			set_status (status, E_ALLOC, 2,
			            "strdup failed");
			free (attr->name);
			return FALSE;
		}
	}

	return TRUE;
}

static inline
void
Attr_raw_init (/*@special@*/ struct SH_Attr * attr,
               /*@only@*/ char * name,
               /*@null@*/ /*@only@*/ char * value)
	/*@defines attr->name,
	           attr->value@*/
	/*@modifies attr->name@*/
	/*@modifies attr->value@*/
{
	attr->name = name;
	attr->value = value;
	return;
}

static inline
bool
Attr_copy (/*@special@*/ struct SH_Attr * copy,
           const struct SH_Attr * attr,
           /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@defines copy->name,
	           copy->value@*/
	/*@modifies copy->name@*/
	/*@modifies copy->value@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{

	copy->name = strdup (attr->name);
	if (NULL == copy->name)
	{
		set_status (status, E_ALLOC, 2, "strdup failed");
		return FALSE;
	}

	if (NULL == attr->value)
	{
		copy->value = NULL;
	}
	else
	{
		copy->value = strdup (attr->value);
		if (NULL == copy->value)
		{
			set_status (status, E_ALLOC, 2,
			            "strdup failed");
			free (copy->name);
			return FALSE;
		}
	}

	return TRUE;
}

static inline
void
Attr_free (/*@special@*/ struct SH_Attr * attr)
	/*@modifies attr->name@*/
	/*@modifies attr->value@*/
	/*@releases attr->name,
	            attr->value@*/
{
	free (attr->name);
	if (NULL != attr->value) free (attr->value);
	return;
}

static inline
void
Attr_move (struct SH_Attr * dest,
           struct SH_Attr * src)
	/*@defines dest->name,
	           dest->value@*/
	/*@modifies dest->name@*/
	/*@modifies dest->value@*/
{

	dest->name = src->name;
	dest->value = src->value;
	return;
}

static inline
/*@observer@*/
char *
Attr_get_name (const struct SH_Attr * attr)
	/*@*/
{
	return attr->name;
}

static inline
void
Attr_set_name (struct SH_Attr * attr, /*@only@*/ char * name)
	/*@modifies attr->name@*/
{
	free (attr->name);
	attr->name = name;
	return;
}

static inline
/*@null@*/
/*@observer@*/
char *
Attr_get_value (const struct SH_Attr * attr)
{
	return attr->value;
}

static inline
void
Attr_set_value (struct SH_Attr * attr, /*@null@*/ /*@only@*/ char * value)
	/*@modifies attr->value@*/
{
	if (NULL != attr->value)
	{
		free (attr->value);
	}

	attr->value = value;
	return;
}

static inline
bool
Attr_has_value (const struct SH_Attr * attr)
	/*@*/
{
	return (NULL != attr->value);
}

static inline
bool
Attr_is_equal (const struct SH_Attr * attr1,
               const struct SH_Attr * attr2)
	/*@*/
{
	if (0 != strcmp (attr1->name, attr2->name)) return FALSE;
	if (NULL == attr1->value) return (NULL == attr2->value);
	if (NULL == attr2->value) return FALSE;
	return (0 == strcmp (attr1->value, attr2->value));
}

static inline
bool
Attr_is_equal_name (const struct SH_Attr * attr1,
                    const struct SH_Attr * attr2)
	/*@*/
{
	return (0 == strcmp (attr1->name, attr2->name));
}

#endif /* SEFHT_ATTR_STATIC_C */
