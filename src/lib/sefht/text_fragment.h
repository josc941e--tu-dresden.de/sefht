/*
 * text_fragment.h
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef SEFHT_TEXT_FRAGMENT_H
#define SEFHT_TEXT_FRAGMENT_H

#if !defined (SEFHT_SEFHT_H_INSIDE) && !defined (SEFHT_COMPILATION)
#status "Only <sefht/sefht.h> can be included directly."
#endif

#include "status.h"

#include "text.h"

#include "fragment.h"


typedef /*@abstract@*/ struct SH_TextFragment SH_TextFragment;


/*@null@*/
/*@only@*/
SH_Fragment /*@alt SH_TextFragment*/ *
SH_TextFragment_new (/*@dependent@*/ SH_Data * data,
                     /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

void
SH_TextFragment_free (/*@only@*/ SH_TextFragment * fragment)
	/*@modifies fragment@*/
	/*@releases fragment@*/;

/*@null@*/
/*@only@*/
SH_Fragment /*@alt SH_TextFragment@*/ *
SH_TextFragment_copy (const SH_TextFragment * fragment,
                      /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

/*@observer@*/
const SH_Text *
SH_TextFragment_get_text (const SH_TextFragment * fragment)
	/*@*/;

bool
SH_TextFragment_append_string (SH_TextFragment * fragment,
                               const char * string,
                               /*@null@*/ /*@out@*/
                               struct SH_Status * status)
	/*@modifies fragment@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_TextFragment_append_text (SH_TextFragment * fragment,
                             const SH_Text * text,
                             /*@null@*/ /*@out@*/
                             struct SH_Status * status)
	/*@modifies fragment@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

void
SH_TextFragment_join (SH_TextFragment * fragment,
                      /*@only@*/ SH_Text * text)
	/*@modifies fragment@*/
	/*@modifies text@*/
	/*@releases text@*/;

void
SH_TextFragment_print (const SH_TextFragment * fragment)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/;

/*@null@*/
/*@only@*/
SH_Text *
SH_TextFragment_to_html (SH_TextFragment * fragment,
                         enum HTML_MODE mode,
                         unsigned int indent_base,
                         unsigned int indent_step,
                         char * indent_char,
                         /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

#endif /* SEFHT_TEXT_FRAGMENT_H */
