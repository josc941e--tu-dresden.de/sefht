/*
 * data.c
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "macro.h"
#include "log.h"
#include "status.h"

#include "validator.h"

#include "data.h"


struct SH_Page
{
	page_t id;
	char * name;
};

struct SH_Data
{
	SH_Validator * validator;
	size_t page_n;
	struct SH_Page * pages;
	page_t last_page;
};


static inline
page_t
next_page (struct SH_Data * data)
	/*@*/
{
	if (data->last_page == PAGE_MAX)
	{
		return PAGE_ERR;
	}
	return data->last_page + 1;
}

static inline
bool
init_validator (/*@special@*/ struct SH_Data * data,
                /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@allocates data->validator@*/
	/*@defines *(data->validator)@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

static inline
void
init_pages (/*@special@*/ struct SH_Data * data)
	/*@allocates data->pages@*/
	/*@defines data->page_n,
	 *         data->last_page@*/
	/*@modifies data->page_n@*/
	/*@modifies data->pages@*/
	/*@modifies data->last_page@*/;

/*@null@*/
/*@only@*/
struct SH_Data *
SH_Data_new (/*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/

{
	struct SH_Data * data;
	data = malloc (sizeof (struct SH_Data));

	if (data == NULL)
	{
		set_status (status, E_ALLOC, 4,
		            "Memory allocation for SH_Data failed.\n");

		return NULL;
	}

	if (!init_validator (data, status))
	{

/* dangerous call to silence splint, should never be executed. */
#ifdef S_SPLINT_S
		free (data->validator);
#endif

		free (data);

		return NULL;
	}

	init_pages (data);

	set_success (status);
	return data;
}

static inline
void
free_validator (/*@special@*/ struct SH_Data * data)
	/*@modifies data->validator@*/
	/*@releases data->validator@*/;

static inline
void
free_pages (/*@special@*/ struct SH_Data * data)
	/*@modifies data->pages@*/
	/*@releases data->pages@*/
	/*@requires maxRead(data->pages) == (data->page_n - 1)@*/;

void
SH_Data_free (/*@only@*/ struct SH_Data * data)
	/*@modifies data->validator@*/
	/*@modifies data->pages@*/
	/*@modifies data@*/
	/*@releases data@*/
{
	free_validator (data);
	free_pages (data);

	free (data);

	return;
}

static inline
bool
init_validator (struct SH_Data * data,
                /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@allocates data->validator@*/
	/*@defines *(data->validator)@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	data->validator = SH_Validator_new (status);

	if (data->validator == NULL)
	{
		return FALSE;
	}

	SH_Validator_register_tag (data->validator, "html", NULL);
	SH_Validator_register_tag (data->validator, "head", NULL);
	SH_Validator_register_tag (data->validator, "body", NULL);
	SH_Validator_register_tag (data->validator, "meta", NULL);
	SH_Validator_register_tag (data->validator, "link", NULL);
	SH_Validator_register_tag (data->validator, "title", NULL);
	SH_Validator_register_tag (data->validator, "main", NULL);
	SH_Validator_register_tag (data->validator, "article", NULL);
	SH_Validator_register_tag (data->validator, "section", NULL);
	SH_Validator_register_tag (data->validator, "header", NULL);
	SH_Validator_register_tag (data->validator, "footer", NULL);
	SH_Validator_register_tag (data->validator, "h1", NULL);
	SH_Validator_register_tag (data->validator, "h2", NULL);
	SH_Validator_register_tag (data->validator, "h3", NULL);
	SH_Validator_register_tag (data->validator, "h4", NULL);
	SH_Validator_register_tag (data->validator, "h5", NULL);
	SH_Validator_register_tag (data->validator, "h6", NULL);
	SH_Validator_register_tag (data->validator, "p", NULL);
	SH_Validator_register_tag (data->validator, "br", NULL);
	SH_Validator_register_tag (data->validator, "i", NULL);
	SH_Validator_register_tag (data->validator, "b", NULL);
	SH_Validator_register_tag (data->validator, "strong", NULL);
	SH_Validator_register_tag (data->validator, "em", NULL);
	SH_Validator_register_tag (data->validator, "small", NULL);

	return TRUE;
}

static inline
void
free_validator (/*@special@*/ struct SH_Data * data)
	/*@modifies data->validator@*/
	/*@releases data->validator@*/
{
	SH_Validator_free (data->validator);
	return;
}

static inline
void
init_pages (/*@special@*/ struct SH_Data * data)
	/*@allocates data->pages@*/
	/*@defines data->page_n,
	 *         data->last_page@*/
	/*@modifies data->page_n@*/
	/*@modifies data->pages@*/
	/*@modifies data->last_page@*/
{
	data->page_n = 0;
	data->pages = malloc (0);
	data->last_page = PAGE_ERR;

	return;
}

static inline
void
free_pages (/*@special@*/ struct SH_Data * data)
	/*@modifies data->pages@*/
	/*@releases data->pages@*/
	/*@requires maxRead(data->pages) == (data->page_n - 1)@*/
{
	size_t index;

	for (index = 0; index < data->page_n; index++)
	{
		free (data->pages[index].name);
	}

	free (data->pages);
	return;
}

page_t
SH_Data_register_page (struct SH_Data * data, const char * name,
                       struct SH_Status * status)
	/*@modifies data->page_n@*/
	/*@modifies data->pages@*/
	/*@modifies data->last_page@*/
	/*@requires maxRead(data->pages) == (data->page_n - 1)@*/
	/*@ensures maxRead(data->pages) == (data->page_n - 1)@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	struct SH_Page * new_pages;
	page_t page_id;

	page_id = next_page (data);
	/* abort on overflow */
	if ((page_id == PAGE_ERR) || (data->page_n == SIZE_MAX))
	{
		set_status (status, E_DOMAIN, 0,
		            "Maximum number of pages reached.\n");

		return PAGE_ERR;
	}

	new_pages = realloc (data->pages,
	                     sizeof (struct SH_Page)
	                     * (data->page_n + 1));

	if (new_pages == NULL)
	{
		set_status (status, E_ALLOC, 5,
		           "Memory allocation for page data failed.\n");

/* bad code to silence splint, should never be executed. */
#ifdef S_SPLINT_S
		data->pages = (void *) 0x12345;
#endif

		return PAGE_ERR;
	}

	new_pages[data->page_n].id = page_id;
	new_pages[data->page_n].name = strdup (name);

	if (new_pages[data->page_n].name == NULL)
	{
		set_status (status, E_ALLOC, 4,
		           "Memory allocation for page data failed.\n");

		data->pages = new_pages;
		return PAGE_ERR;
	}

	/* commit changes */
	data->pages = new_pages;
	data->last_page = page_id;
	data->page_n++;

	set_success (status);

	return data->last_page;
}
