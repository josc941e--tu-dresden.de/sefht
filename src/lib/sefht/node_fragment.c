/*
 * node_fragment.c
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "macro.h"
#include "log.h"
#include "status.h"

#include "attr_static.c"
#include "data.h"
#include "text.h"
#include "validator.h"

#include "fragment.h"

#include "node_fragment.h"


#include "fragment_class.c"

#define CHILD_CHUNK 5
#define ATTR_CHUNK 5


struct SH_NodeFragment
{
	struct SH_Fragment base;

	/*@only@*/ char * tag;

	size_t attr_s; /* allocated size */
	size_t attr_n; /* real size */
	/*@only@*/ struct SH_Attr * attrs;

	size_t child_s; /* allocated size */
	size_t child_n; /* real size */
	/*@only@*/ struct SH_Fragment ** childs;
};

#define OPEN_TAG_BEGIN "<"
#define OPEN_TAG_END ">"
#define CLOSE_TAG_BEGIN "</"
#define CLOSE_TAG_END ">"

#define ATTR_NAME_DELIMITER " "
#define ATTR_VALUE_DELIMITER "="
#define ATTR_VALUE_QUOTE_BEGIN "\""
#define ATTR_VALUE_QUOTE_END "\""

#define NEWLINE "\n"


static const struct fragment_methods methods = {
	(struct SH_Fragment * (*)(const struct SH_Fragment *,
	                          /*@out@*/ /*@null@*/
	                          struct SH_Status *))
	SH_NodeFragment_deepcopy,

	(void (*)(/*@only@*/ struct SH_Fragment *))
	SH_NodeFragment_free,

	(struct SH_Text * (*)(const struct SH_Fragment *,
	                      enum HTML_MODE,
	                      unsigned int, unsigned int, const char *,
	                      /*@out@*/ /*@null@*/ struct SH_Status *))
	SH_NodeFragment_to_html
};


static inline
size_t
get_child_alloc_size (size_t size)
	/*@*/
{
	/* underflow */
	if (size == 0)
	{
		return 0;
	}
	/* overflow */
	else if (((SIZE_MAX / CHILD_CHUNK) - 1)
	       < ((size - 1) / CHILD_CHUNK))
	{
		return SIZE_MAX;
	}
	/* calculate the number of needed chunks */
	else
	{
		return CHILD_CHUNK * (((size - 1) / CHILD_CHUNK) + 1);
	}
}

static inline
size_t
get_attr_alloc_size (size_t size)
	/*@*/
{
	/* underflow */
	if (size == 0)
	{
		return 0;
	}
	/* overflow */
	else if (((SIZE_MAX / ATTR_CHUNK) - 1)
	       < ((size - 1) / ATTR_CHUNK))
	{
		return SIZE_MAX;
	}
	/* calculate the number of needed chunks */
	else
	{
		return ATTR_CHUNK * (((size - 1) / ATTR_CHUNK) + 1);
	}
}


/*@null@*/
/*@only@*/
struct SH_Fragment /*@alt struct SH_NodeFragment@*/ *
SH_NodeFragment_new (const char * tag,
                     /*@dependent@*/ SH_Data * data,
                     /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@globals NODE,
	           fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	struct SH_NodeFragment * fragment;

	fragment = malloc (sizeof (struct SH_NodeFragment));
	if (fragment == NULL)
	{
		set_status (status, E_ALLOC, 3,
		            "Memory allocation for "
		            "SH_NodeFragment failed.\n");

		return NULL;
	}

	init_fragment (&(fragment->base), NODE, &methods, data, NULL);

	fragment->tag = strdup (tag);
	if (fragment->tag == NULL)
	{
		set_status (status, E_ALLOC, 3,
		            "Memory allocation for "
		            "fragment tag failed.\n");

		free (fragment);

		return NULL;
	}


	fragment->attr_s = 0;
	fragment->attr_n = 0;
	fragment->attrs = malloc (0);

	fragment->child_s = 0;
	fragment->child_n = 0;
	fragment->childs = malloc (0);

	set_success (status);

	return (struct SH_Fragment *) fragment;
}

/*@null@*/
/*@only@*/
struct SH_Fragment /*@alt struct SH_NodeFragment@*/ *
SH_NodeFragment_raw_new (/*@only@*/ char * tag,
                         /*@dependent@*/ SH_Data * data,
                         /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@globals NODE,
	           fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	struct SH_NodeFragment * fragment;

	fragment = malloc (sizeof (struct SH_NodeFragment));
	if (fragment == NULL)
	{
		set_status (status, E_ALLOC, 3,
		            "Memory allocation for "
		            "SH_NodeFragment failed.\n");

		return NULL;
	}

	init_fragment (&(fragment->base), NODE, &methods, data, NULL);

	fragment->tag = tag;

	fragment->attr_s = 0;
	fragment->attr_n = 0;
	fragment->attrs = malloc (0);

	fragment->child_s = 0;
	fragment->child_n = 0;
	fragment->childs = malloc (0);

	set_success (status);

	return (struct SH_Fragment *) fragment;
}

void
SH_NodeFragment_free (/*@only@*/ struct SH_NodeFragment * fragment)
	/*@modifies fragment@*/
	/*@releases fragment@*/
{
	size_t index;

	free (fragment->tag);

	for (index = 0; index < fragment->attr_n; index++)
	{
		Attr_free (&fragment->attrs[index]);
	}
	free (fragment->attrs);

	for (index = 0; index < fragment->child_n; index++)
	{
		SH_Fragment_free (fragment->childs[index]);
	}
	free (fragment->childs);

	free_fragment (&fragment->base);

	free (fragment);

	return;
}

/*@null@*/
/*@only@*/
struct SH_Fragment /*@alt struct SH_NodeFragment@*/ *
SH_NodeFragment_copy (const struct SH_NodeFragment * fragment,
                      /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	struct SH_NodeFragment * copy;
	size_t index;

	copy = malloc (sizeof (struct SH_NodeFragment));
	if (copy == NULL)
	{
		set_status (status, E_ALLOC, 3,
		            "Memory allocation for "
		            "SH_NodeFragment failed.\n");

		return NULL;
	}

	copy_fragment (&(copy->base), &(fragment->base));

	/* copy tag */
	copy->tag = strdup (fragment->tag);
	if (copy->tag == NULL)
	{
		set_status (status, E_ALLOC, 3,
		            "Memory allocation for "
		            "fragment tag failed.\n");

		free (copy);

		return NULL;
	}

	/* copy attributes */
	copy->attr_n = fragment->attr_n;
	copy->attr_s = get_attr_alloc_size (fragment->attr_n);
	copy->attrs = malloc (copy->attr_s * sizeof (*copy->attrs));

	if (copy->attr_s != 0 && copy->attrs == NULL)
	{
		set_status (status, E_ALLOC, 5, "malloc failed\n");

		free (copy->tag);
/* dangerous call to silence splint, should never be executed. */
#ifdef S_SPLINT_S
		free (copy->attrs);
#endif
		free (copy);

		return NULL;
	}

	for (index = 0; index < fragment->attr_n; index++)
	{
		if (!Attr_copy (&copy->attrs[index],
		                &fragment->attrs[index],
		                status))
		{
			while (index > 0)
			{
				index--;
				Attr_free (&copy->attrs[index]);
			}

			free (copy->attrs);
			free (copy->tag);
			free (copy);
			return NULL;
		}
	}

	/* don't copy childs */
	copy->child_s = 0;
	copy->child_n = 0;
	copy->childs = malloc (0);

	set_success (status);

	return (struct SH_Fragment *) copy;
}

/*@null@*/
/*@only@*/
struct SH_Fragment /*@alt struct SH_NodeFragment@*/ *
SH_NodeFragment_deepcopy (const struct SH_NodeFragment * fragment,
                          /*@out@*/ /*@null@*/
                          struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	struct SH_NodeFragment * copy;
	struct SH_Fragment * child;
	size_t index;

	copy = malloc (sizeof (struct SH_NodeFragment));
	if (copy == NULL)
	{
		set_status (status, E_ALLOC, 3,
		            "Memory allocation for "
		            "SH_NodeFragment failed.\n");

		return NULL;
	}

	copy_fragment (&(copy->base), &(fragment->base));

	/* copy tag */
	copy->tag = strdup (fragment->tag);
	if (copy->tag == NULL)
	{
		set_status (status, E_ALLOC, 3,
		            "Memory allocation for "
		            "fragment tag failed.\n");

		free (copy);

		return NULL;
	}

	/* copy attributes */
	copy->attr_n = fragment->attr_n;
	copy->attr_s = get_attr_alloc_size (fragment->attr_n);
	copy->attrs = malloc (copy->attr_s * sizeof (*copy->attrs));

	if (copy->attr_s != 0 && copy->attrs == NULL)
	{
		set_status (status, E_ALLOC, 5, "malloc failed\n");

		free (copy->tag);
/* dangerous call to silence splint, should never be executed. */
#ifdef S_SPLINT_S
		free (copy->attrs);
#endif
		free (copy);

		return NULL;
	}

	for (index = 0; index < fragment->attr_n; index++)
	{
		if (!Attr_copy (&copy->attrs[index],
		                &fragment->attrs[index],
		                status))
		{
			while (index > 0)
			{
				index--;
				Attr_free (&copy->attrs[index]);
			}

			free (copy->attrs);
			free (copy->tag);
			free (copy);
			return NULL;
		}
	}

	/* copy childs */
	copy->child_n = fragment->child_n;
	copy->child_s = get_child_alloc_size (fragment->child_n);
	copy->childs = malloc (copy->child_s * sizeof (*copy->childs));

	if (copy->child_s != 0 && copy->childs == NULL)
	{
		set_status (status, E_ALLOC, 5,
		            "Memory allocation for "
		            "fragment child failed.\n");

		free (copy->tag);
/* dangerous call to silence splint, should never be executed. */
#ifdef S_SPLINT_S
		free (copy->childs);
#endif
		free (copy->attrs);
		free (copy);

		return NULL;
	}

	for (index = 0; index < fragment->child_n; index++)
	{
		child = SH_Fragment_copy (fragment->childs[index],
		                          status);

		if (child == NULL)
		{
			copy->child_n = index;
			SH_NodeFragment_free (copy);

			return NULL;
		}

		copy->childs[index] = child;
		child->parent = copy;
	}

	set_success (status);

	return (struct SH_Fragment *) copy;
}

bool
SH_Fragment_is_NodeFragment (const struct SH_Fragment * fragment)
	/*@*/
{
	return get_type (fragment) == NODE;
}

static inline
/*@observer@*/
char *
get_tag (const struct SH_NodeFragment * fragment)
	/*@*/
{
	return fragment->tag;
}

/*@null@*/
/*@only@*/
char *
SH_NodeFragment_get_tag (const struct SH_NodeFragment * fragment,
                         /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	char * tag;

	tag = strdup (get_tag (fragment));

	if (tag == NULL)
	{
		set_status (status, E_ALLOC, 4,
		            "Memory allocation for "
		            "fragment tag failed.\n");

		return NULL;
	}

	set_success (status);

	return tag;
}

/*@observer@*/
char *
SH_NodeFragment_raw_get_tag (const struct SH_NodeFragment * fragment)
{
	return get_tag (fragment);
}

/*@null@*/
/*@dependent@*/
struct SH_NodeFragment *
SH_Fragment_get_parent (const struct SH_Fragment * fragment)
	/*@*/
{
	return get_parent (fragment);
}

size_t
SH_NodeFragment_count_attrs (const struct SH_NodeFragment * fragment)
	/*@*/
{
	return fragment->attr_n;
}

/*@null@*/
/*@observer@*/
const SH_Attr *
SH_NodeFragment_get_attr (const struct SH_NodeFragment * fragment,
                          size_t index,
                          /*@out@*/ /*@null@*/
                          struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	if (index >= fragment->attr_n)
	{
		set_status (status, E_VALUE, 2,
		            "Fragment: Attr index out of range.\n");

		return NULL;
	}

	set_success (status);

	return &(fragment->attrs[index]);
}

static inline
bool
insert_attr (struct SH_NodeFragment * fragment,
             /*@only@*/ SH_Attr * attr,
             size_t position,
             /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@modifies fragment->attrs@*/
	/*@modifies fragment->attr_s@*/
	/*@modifies fragment->attr_n@*/
	/*@modifies attr@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	size_t new_size;
	size_t index;
	typeof (fragment->attrs) new_attrs;

	new_size = get_attr_alloc_size (fragment->attr_n + 1);
	if (new_size > fragment->attr_s)
	{
		if ((SIZE_MAX / sizeof (*new_attrs)) < new_size)
		{
			set_status (status, E_DOMAIN, -6,
			            "maximum number of "
			            "attrs reached.\n");
			return FALSE;
		}

		new_attrs = realloc (fragment->attrs, new_size
		                      * sizeof (*new_attrs));

		if (new_attrs == NULL)
		{
			set_status (status, E_ALLOC, 5,
			            "malloc failed\n");

/* bad code to silence splint, should never be executed. */
#ifdef S_SPLINT_S
			fragment->attrs = (void *) 0x12345;
#endif
			return FALSE;
		}

		fragment->attrs = new_attrs;
		fragment->attr_s = new_size;
	}

	for (index = fragment->attr_n; index > position; index--)
	{
		fragment->attrs[index] = fragment->attrs[index-1];
	}
	Attr_move (&(fragment->attrs[position]), attr);
	free (attr);
	fragment->attr_n++;

	set_success (status);

	return TRUE;
}

static inline
bool
insert_attr_new (struct SH_NodeFragment * fragment,
                 const char * name,
                 /*@null@*/ const char * value,
                 size_t position,
                 /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@modifies fragment->attrs@*/
	/*@modifies fragment->attr_s@*/
	/*@modifies fragment->attr_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	size_t new_size;
	size_t index;
	struct SH_Attr attr;
	typeof (fragment->attrs) new_attrs;

	if (!Attr_init (&attr, name, value, status))
	{
		return FALSE;
	}

	new_size = get_attr_alloc_size (fragment->attr_n + 1);
	if (new_size > fragment->attr_s)
	{
		if ((SIZE_MAX / sizeof (*new_attrs)) < new_size)
		{
			set_status (status, E_DOMAIN, -6,
			            "maximum number of "
			            "attrs reached.\n");
			Attr_free (&attr);
			return FALSE;
		}

		new_attrs = realloc (fragment->attrs, new_size
		                      * sizeof (*new_attrs));

		if (new_attrs == NULL)
		{
			set_status (status, E_ALLOC, 5,
			            "malloc failed\n");

/* bad code to silence splint, should never be executed. */
#ifdef S_SPLINT_S
			fragment->attrs = (void *) 0x12345;
#endif
			Attr_free (&attr);
			return FALSE;
		}

		fragment->attrs = new_attrs;
		fragment->attr_s = new_size;
	}

	for (index = fragment->attr_n; index > position; index--)
	{
		fragment->attrs[index] = fragment->attrs[index-1];
	}
	Attr_move (&(fragment->attrs[position]), &attr);
	fragment->attr_n++;

	set_success (status);

	return TRUE;
}

static inline
bool
insert_attr_raw_new (struct SH_NodeFragment * fragment,
                     /*@only@*/ char * name,
                     /*@null@*/ /*@only@*/ char * value,
                     size_t position,
                     /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@modifies fragment->attrs@*/
	/*@modifies fragment->attr_s@*/
	/*@modifies fragment->attr_n@*/
	/*@modifies name@*/
	/*@modifies value@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	size_t new_size;
	size_t index;
	typeof (fragment->attrs) new_attrs;

	new_size = get_attr_alloc_size (fragment->attr_n + 1);
	if (new_size > fragment->attr_s)
	{
		if ((SIZE_MAX / sizeof (*new_attrs)) < new_size)
		{
			set_status (status, E_DOMAIN, -6,
			            "maximum number of "
			            "attrs reached.\n");
			return FALSE;
		}

		new_attrs = realloc (fragment->attrs, new_size
		                      * sizeof (*new_attrs));

		if (new_attrs == NULL)
		{
			set_status (status, E_ALLOC, 5,
			            "malloc failed\n");

/* bad code to silence splint, should never be executed. */
#ifdef S_SPLINT_S
			fragment->attrs = (void *) 0x12345;
#endif
			return FALSE;
		}

		fragment->attrs = new_attrs;
		fragment->attr_s = new_size;
	}

	for (index = fragment->attr_n; index > position; index--)
	{
		fragment->attrs[index] = fragment->attrs[index-1];
	}
	Attr_raw_init (&(fragment->attrs[position]), name, value);
	fragment->attr_n++;

	set_success (status);

	return TRUE;
}

static inline
bool
insert_attr_copy (struct SH_NodeFragment * fragment,
                  const SH_Attr * attr,
                  size_t position,
                  /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@modifies fragment->attrs@*/
	/*@modifies fragment->attr_s@*/
	/*@modifies fragment->attr_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	size_t new_size;
	size_t index;
	typeof (fragment->attrs) new_attrs;

	new_size = get_attr_alloc_size (fragment->attr_n + 1);
	if (new_size > fragment->attr_s)
	{
		if ((SIZE_MAX / sizeof (*new_attrs)) < new_size)
		{
			set_status (status, E_DOMAIN, -6,
			            "maximum number of "
			            "attrs reached.\n");
			return FALSE;
		}

		new_attrs = realloc (fragment->attrs, new_size
		                      * sizeof (*new_attrs));

		if (new_attrs == NULL)
		{
			set_status (status, E_ALLOC, 5,
			            "malloc failed\n");

/* bad code to silence splint, should never be executed. */
#ifdef S_SPLINT_S
			fragment->attrs = (void *) 0x12345;
#endif
			return FALSE;
		}

		fragment->attrs = new_attrs;
		fragment->attr_s = new_size;
	}

	for (index = fragment->attr_n; index > position; index--)
	{
		fragment->attrs[index] = fragment->attrs[index-1];
	}

	if (!Attr_copy (&(fragment->attrs[position]), attr, status))
	{
		for (index = position; index < fragment->attr_n; index++)
		{
			fragment->attrs[index] = fragment->attrs[index+1];
		}
		return FALSE;
	}

	fragment->attr_n++;

	set_success (status);

	return TRUE;
}

bool
SH_NodeFragment_insert_attr (struct SH_NodeFragment * fragment,
                             /*@only@*/ SH_Attr * attr,
                             size_t position,
                             /*@out@*/ /*@null@*/
                             struct SH_Status * status)
	/*@modifies fragment->attrs@*/
	/*@modifies fragment->attr_s@*/
	/*@modifies fragment->attr_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	if (position > fragment->attr_n)
	{
		set_status (status, E_VALUE, 2,
		            "index out of range.\n");
		return FALSE;
	}

	return insert_attr (fragment, attr, position, status);
}

bool
SH_NodeFragment_insert_attr_new (struct SH_NodeFragment * fragment,
                                 const char * name,
                                 /*@null@*/ const char * value,
                                 size_t position,
                                 /*@out@*/ /*@null@*/
                                 struct SH_Status * status)
	/*@modifies fragment->attrs@*/
	/*@modifies fragment->attr_s@*/
	/*@modifies fragment->attr_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	if (position > fragment->attr_n)
	{
		set_status (status, E_VALUE, 2,
		            "index out of range.\n");
		return FALSE;
	}

	return insert_attr_new (fragment, name, value, position, status);
}

bool
SH_NodeFragment_insert_attr_raw_new (struct SH_NodeFragment * fragment,
                                     /*@only@*/ char * name,
                                     /*@null@*/ /*@only@*/ char * value,
                                     size_t position,
                                     /*@out@*/ /*@null@*/
                                     struct SH_Status * status)
	/*@modifies fragment->attrs@*/
	/*@modifies fragment->attr_s@*/
	/*@modifies fragment->attr_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	if (position > fragment->attr_n)
	{
		set_status (status, E_VALUE, 2,
		            "index out of range.\n");
		return FALSE;
	}

	return insert_attr_raw_new (fragment, name, value, position,
	                            status);
}

bool
SH_NodeFragment_insert_attr_copy (struct SH_NodeFragment * fragment,
                                  const SH_Attr * attr,
                                  size_t position,
                                  /*@out@*/ /*@null@*/
                                  struct SH_Status * status)
	/*@modifies fragment->attrs@*/
	/*@modifies fragment->attr_s@*/
	/*@modifies fragment->attr_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	if (position > fragment->attr_n)
	{
		set_status (status, E_VALUE, 2,
		            "index out of range.\n");
		return FALSE;
	}

	return insert_attr_copy (fragment, attr, position, status);
}

bool
SH_NodeFragment_prepend_attr (struct SH_NodeFragment * fragment,
                              /*@only@*/ SH_Attr * attr,
                              /*@out@*/ /*@null@*/
                              struct SH_Status * status)
	/*@modifies fragment->attrs@*/
	/*@modifies fragment->attr_s@*/
	/*@modifies fragment->attr_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	return insert_attr (fragment, attr, 0, status);
}

bool
SH_NodeFragment_prepend_attr_new (struct SH_NodeFragment * fragment,
                                  const char * name,
                                  /*@null@*/ const char * value,
                                  /*@out@*/ /*@null@*/
                                  struct SH_Status * status)
	/*@modifies fragment->attrs@*/
	/*@modifies fragment->attr_s@*/
	/*@modifies fragment->attr_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	return insert_attr_new (fragment, name, value, 0, status);
}

bool
SH_NodeFragment_prepend_attr_raw_new (struct SH_NodeFragment * fragment,
                                      /*@only@*/ char * name,
                                      /*@null@*/ /*@only@*/ char * value,
                                      /*@out@*/ /*@null@*/
                                      struct SH_Status * status)
	/*@modifies fragment->attrs@*/
	/*@modifies fragment->attr_s@*/
	/*@modifies fragment->attr_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	return insert_attr_raw_new (fragment, name, value, 0, status);
}

bool
SH_NodeFragment_prepend_attr_copy (struct SH_NodeFragment * fragment,
                                   const SH_Attr * attr,
                                   /*@out@*/ /*@null@*/
                                   struct SH_Status * status)
	/*@modifies fragment->attrs@*/
	/*@modifies fragment->attr_s@*/
	/*@modifies fragment->attr_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	return insert_attr_copy (fragment, attr, 0, status);
}

bool
SH_NodeFragment_append_attr (struct SH_NodeFragment * fragment,
                             /*@only@*/ SH_Attr * attr,
                             /*@out@*/ /*@null@*/
                             struct SH_Status * status)
	/*@modifies fragment->attrs@*/
	/*@modifies fragment->attr_s@*/
	/*@modifies fragment->attr_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	return insert_attr (fragment, attr, fragment->attr_n, status);
}

bool
SH_NodeFragment_append_attr_new (struct SH_NodeFragment * fragment,
                                 const char * name,
                                 /*@null@*/ const char * value,
                                 /*@out@*/ /*@null@*/
                                 struct SH_Status * status)
	/*@modifies fragment->attrs@*/
	/*@modifies fragment->attr_s@*/
	/*@modifies fragment->attr_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	return insert_attr_new (fragment, name, value,
	                        fragment->attr_n, status);
}

bool
SH_NodeFragment_append_attr_raw_new (struct SH_NodeFragment * fragment,
                                     /*@only@*/ char * name,
                                     /*@null@*/ /*@only@*/ char * value,
                                     /*@out@*/ /*@null@*/
                                     struct SH_Status * status)
	/*@modifies fragment->attrs@*/
	/*@modifies fragment->attr_s@*/
	/*@modifies fragment->attr_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	return insert_attr_raw_new (fragment, name, value,
	                            fragment->attr_n, status);
}

bool
SH_NodeFragment_append_attr_copy (struct SH_NodeFragment * fragment,
                                  const SH_Attr * attr,
                                  /*@out@*/ /*@null@*/
                                  struct SH_Status * status)
	/*@modifies fragment->attrs@*/
	/*@modifies fragment->attr_s@*/
	/*@modifies fragment->attr_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	return insert_attr_copy (fragment, attr, fragment->attr_n,
	                         status);
}

static inline
bool
remove_attr (struct SH_NodeFragment * fragment, size_t position,
             /*@out@*/ /*@null@*/ SH_Attr * attr_ptr,
             /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@modifies fragment->attrs@*/
	/*@modifies fragment->attr_s@*/
	/*@modifies fragment->attr_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	size_t index;
	size_t new_size;
	typeof (fragment->attrs) new_attrs;
	SH_Attr attr;

	Attr_move (&attr, &(fragment->attrs[position]));

	for (index = position+1; index < fragment->attr_n; index++)
	{
		fragment->attrs[index-1] = fragment->attrs[index];
	}

	new_size = get_attr_alloc_size (fragment->attr_n - 1);
	if (new_size < fragment->attr_s)
	{
		new_attrs = realloc (fragment->attrs, new_size
		                      * sizeof (*new_attrs));
		if ((0 != new_size) && (NULL == new_attrs))
		{
			for (index = fragment->attr_n-1;
			     index > position;
			     index--)
			{
				fragment->attrs[index] =
				fragment->attrs[index-1];
			}
			fragment->attrs[position] = attr;

			set_status (status, E_ALLOC, 4,
			            "realloc failed.\n");
			return FALSE;
		}

		fragment->attrs = new_attrs;
		fragment->attr_s = new_size;
	}

	fragment->attr_n--;

	if (NULL == attr_ptr)
	{
		Attr_free (&attr);
	}
	else
	{
		*attr_ptr = attr;
	}

	set_success (status);
	return TRUE;
}

bool
SH_NodeFragment_remove_attr (struct SH_NodeFragment * fragment,
                             size_t position,
                             /*@out@*/ /*@null@*/
                             struct SH_Status * status)
	/*@modifies fragment->attrs@*/
	/*@modifies fragment->attr_s@*/
	/*@modifies fragment->attr_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	if (position >= fragment->attr_n)
	{
		set_status (status, E_VALUE, 2,
		            "index out of range.\n");
		return FALSE;
	}

	return remove_attr (fragment, position, NULL, status);
}

/*@null@*/
/*@only@*/
SH_Attr *
SH_NodeFragment_pop_attr (struct SH_NodeFragment * fragment,
                          size_t position,
                          /*@out@*/ /*@null@*/
                          struct SH_Status * status)
	/*@modifies fragment->attrs@*/
	/*@modifies fragment->attr_s@*/
	/*@modifies fragment->attr_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	SH_Attr * attr;

	if (position >= fragment->attr_n)
	{
		set_status (status, E_VALUE, 2,
		            "index out of range.\n");
		return NULL;
	}

	attr = malloc (sizeof (SH_Attr));
	if (NULL == attr)
	{
		set_status (status, E_ALLOC, 3, "malloc failed");
		return NULL;
	}

	if (!remove_attr (fragment, position, attr, status))
	{
		return NULL;
	}

	return attr;
}

size_t
SH_NodeFragment_count_childs (const struct SH_NodeFragment * fragment)
	/*@*/
{
	return fragment->child_n;
}

/*@null@*/
/*@observer@*/
const struct SH_Fragment *
SH_NodeFragment_get_child (const struct SH_NodeFragment * fragment,
                           size_t index,
                           /*@out@*/ /*@null@*/
                           struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	if (index >= fragment->child_n)
	{
		set_status (status, E_VALUE, 2,
		            "Fragment: Child index out of range.\n");

		return NULL;
	}

	set_success (status);

	return fragment->childs[index];
}

bool
SH_NodeFragment_is_child (const struct SH_NodeFragment * fragment,
                          const struct SH_Fragment * child)
	/*@*/
{
	size_t index;

	for (index = 0; index < fragment->child_n; index++)
	{
		if (fragment->childs[index] == child)
		{
			return TRUE;
		}
	}
	return FALSE;
}

bool
SH_NodeFragment_is_descendant (const struct SH_NodeFragment * fragment,
                          const struct SH_Fragment * child)
	/*@*/
{
	size_t index;

	for (index = 0; index < fragment->child_n; index++)
	{
		if (fragment->childs[index] == child
		|| (SH_Fragment_is_NodeFragment (child)
		    && SH_NodeFragment_is_descendant (
		                        (struct SH_NodeFragment *)
		                        fragment->childs[index],
		                        child)))
		{
			return TRUE;
		}
	}
	return FALSE;
}

bool
SH_NodeFragment_is_parent (const struct SH_Fragment * fragment,
                           const struct SH_NodeFragment * parent)
	/*@*/
{
	if (parent == get_parent (fragment))
	{
		return TRUE;
	}

	return FALSE;
}

bool
SH_NodeFragment_is_ancestor (const struct SH_Fragment * fragment,
                             const struct SH_NodeFragment * ancestor)
	/*@*/
{
	if ((NULL != get_parent (fragment))
	&& ((ancestor == get_parent (fragment))
	   || (SH_NodeFragment_is_ancestor ((const struct SH_Fragment *)
	                                    get_parent (fragment),
	                                    ancestor))))
	{
		return TRUE;
	}

	return FALSE;
}

static inline
bool
insert_child (struct SH_NodeFragment * fragment,
              /*@only@*/ struct SH_Fragment * child,
              size_t position,
              /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@modifies fragment->childs@*/
	/*@modifies fragment->child_s@*/
	/*@modifies fragment->child_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	size_t new_size;
	size_t index;
	struct SH_Fragment ** new_childs;

	if (!SH_Fragment_is_orphan (child))
	{
		set_status (status, E_STATE, 2,
		            "child is already linked; "
		            "please unlink first or link a copy.\n");
		return FALSE;
	}

	if (SH_Fragment_is_NodeFragment (child)
	&& SH_NodeFragment_is_ancestor ((struct SH_Fragment *)fragment,
	                                (struct SH_NodeFragment *)child))
	{
		set_status (status, E_STATE, 2,
		            "refusing to make a tree cyclic.\n");
		return FALSE;
	}

	new_size = get_child_alloc_size (fragment->child_n + 1);
	if (new_size > fragment->child_s)
	{
		if ((SIZE_MAX / sizeof (*new_childs)) < new_size)
		{
			set_status (status, E_DOMAIN, -6,
			            "maximum number of "
			            "childs reached.\n");
			return FALSE;
		}

		new_childs = realloc (fragment->childs, new_size
		                      * sizeof (*new_childs));

		if (new_childs == NULL)
		{
			set_status (status, E_ALLOC, 5,
				    "Memory allocation for "
				    "fragment child failed.\n");

/* bad code to silence splint, should never be executed. */
#ifdef S_SPLINT_S
			fragment->childs = (void *) 0x12345;
#endif
			return FALSE;
		}

		fragment->childs = new_childs;
		fragment->child_s = new_size;
	}

	for (index = fragment->child_n; index > position; index--)
	{
		fragment->childs[index] = fragment->childs[index-1];
	}
	fragment->childs[position] = child;
	fragment->child_n++;
	child->parent = fragment;

	set_success (status);

	return TRUE;
}

bool
SH_NodeFragment_insert_child (struct SH_NodeFragment * fragment,
                              /*@only@*/ struct SH_Fragment * child,
                              size_t position,
                              /*@out@*/ /*@null@*/
                              struct SH_Status * status)
	/*@modifies fragment->childs@*/
	/*@modifies fragment->child_s@*/
	/*@modifies fragment->child_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	if (position > fragment->child_n)
	{
		set_status (status, E_VALUE, 2,
		            "index out of range.\n");
		return FALSE;
	}

	return insert_child (fragment, child, position, status);
}

bool
SH_NodeFragment_prepend_child (struct SH_NodeFragment * fragment,
                              /*@only@*/ struct SH_Fragment * child,
                              /*@out@*/ /*@null@*/
                              struct SH_Status * status)
	/*@modifies fragment->childs@*/
	/*@modifies fragment->child_s@*/
	/*@modifies fragment->child_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	return insert_child (fragment, child, 0, status);
}

bool
SH_NodeFragment_append_child (struct SH_NodeFragment * fragment,
                              /*@only@*/ struct SH_Fragment * child,
                              /*@out@*/ /*@null@*/
                              struct SH_Status * status)
	/*@modifies fragment->childs@*/
	/*@modifies fragment->child_s@*/
	/*@modifies fragment->child_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	return insert_child (fragment, child, fragment->child_n, status);
}

bool
SH_NodeFragment_insert_child_before (struct SH_Fragment * fragment,
                                     /*@only@*/
                                     struct SH_Fragment * child,
                                     /*@out@*/ /*@null@*/
                                     struct SH_Status * status)
	/*@modifies fragment->childs@*/
	/*@modifies fragment->child_s@*/
	/*@modifies fragment->child_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	size_t index;

	#define parent get_parent (fragment)
	if (NULL == parent)
	{
		set_status (status, E_STATE, 2,
		            "child is not linked, "
		            "can't insert before.\n");
		return FALSE;
	}

	for (index = 0; index < parent->child_n; index++)
	{
		if (parent->childs[index] == fragment)
		{
			return insert_child (parent, child,
			                     index, status);
		}
	}
	#undef parent

	set_status (status, E_BUG, 10,
	            "fragment is both child and not child.\n");
	return FALSE;
}

bool
SH_NodeFragment_insert_child_after (struct SH_Fragment * fragment,
                                    /*@only@*/
                                    struct SH_Fragment * child,
                                    /*@out@*/ /*@null@*/
                                    struct SH_Status * status)
	/*@modifies fragment->childs@*/
	/*@modifies fragment->child_s@*/
	/*@modifies fragment->child_n@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	size_t index;

	#define parent get_parent (fragment)
	if (NULL == parent)
	{
		set_status (status, E_STATE, 2,
		            "child is not linked, "
		            "can't insert before.\n");
		return FALSE;
	}

	for (index = 0; index < parent->child_n; index++)
	{
		if (parent->childs[index] == fragment)
		{
			return insert_child (parent, child,
			                     index+1, status);
		}
	}
	#undef parent

	set_status (status, E_BUG, 10,
	            "fragment is both child and not child.\n");
	return FALSE;
}

static inline
/*@null@*/
/*@only@*/
struct SH_Fragment *
remove_child (struct SH_NodeFragment * fragment, size_t position,
              /*@out@*/ /*@null@*/ struct SH_Status * status)
{
	size_t index;
	size_t new_size;
	typeof (fragment->childs) new_childs;
	struct SH_Fragment * child;

	child = fragment->childs[position];

	for (index = position+1; index < fragment->child_n; index++)
	{
		fragment->childs[index-1] = fragment->childs[index];
	}

	new_size = get_child_alloc_size (fragment->child_n - 1);
	if (new_size < fragment->child_s)
	{
		new_childs = realloc (fragment->childs, new_size
		                      * sizeof (*new_childs));
		if ((0 != new_size) && (NULL == new_childs))
		{
			for (index = fragment->child_n-1;
			     index > position;
			     index--)
			{
				fragment->childs[index] =
				fragment->childs[index-1];
			}
			fragment->childs[position] = child;

			set_status (status, E_ALLOC, 4,
			            "realloc failed.\n");
			return NULL;
		}

		fragment->childs = new_childs;
		fragment->child_s = new_size;
	}

	fragment->child_n--;
	child->parent = NULL;

	set_success (status);

	return child;
}

bool
SH_Fragment_remove (struct SH_Fragment * fragment,
                    /*@out@*/ /*@null@*/ struct SH_Status * status)
{
	size_t index;

	#define parent get_parent (fragment)
	if (NULL == parent)
	{
		set_status (status, E_STATE, 2,
		            "fragment not linked, "
		            "can't remove it from parent");
		return FALSE;
	}

	for (index = 0; index < parent->child_n; index++)
	{
		if (fragment == parent->childs[index])
		{
			return NULL != remove_child (parent, index,
			                             status);
		}
	}
	#undef parent

	set_status (status, E_BUG, 10,
	            "fragment is both child and not child.\n");
	return FALSE;
}

bool
SH_Fragment_delete (/*@only@*/ struct SH_Fragment * fragment,
                    /*@out@*/ /*@null@*/ struct SH_Status * status)
{
	size_t index;

	#define parent get_parent (fragment)
	if (NULL == parent)
	{
		set_status (status, E_STATE, 2,
		            "fragment not linked, "
		            "can't remove it from parent");
		return FALSE;
	}

	for (index = 0; index < parent->child_n; index++)
	{
		if (fragment == parent->childs[index])
		{
			if (NULL == remove_child (parent, index, status))
			{
				return FALSE;
			}

			SH_Fragment_free (fragment);
			return TRUE;
		}
	}
	#undef parent

	set_status (status, E_BUG, 10,
	            "fragment is both child and not child.\n");
	return FALSE;
}

bool
SH_NodeFragment_remove_child (struct SH_NodeFragment * fragment,
                              size_t position,
                              /*@out@*/ /*@null@*/
                              struct SH_Status * status)
{
	if (position >= fragment->child_n)
	{
		set_status (status, E_VALUE, 2,
		            "index out of range.\n");
		return FALSE;
	}

	return NULL != remove_child (fragment, position, status);
}

bool
SH_NodeFragment_delete_child (struct SH_NodeFragment * fragment,
                              size_t position,
                              /*@out@*/ /*@null@*/
                              struct SH_Status * status)
{
	struct SH_Fragment * child;

	if (position >= fragment->child_n)
	{
		set_status (status, E_VALUE, 2,
		            "index out of range.\n");
		return FALSE;
	}

	child = remove_child (fragment, position, status);
	if (NULL == child)
	{
		return FALSE;
	}

	SH_Fragment_free (child);
	return TRUE;
}

/*@null@*/
/*@only@*/
struct SH_Fragment *
SH_NodeFragment_pop_child (struct SH_NodeFragment * fragment,
                           size_t position,
                           /*@out@*/ /*@null@*/
                           struct SH_Status * status)
{
	if (position >= fragment->child_n)
	{
		set_status (status, E_VALUE, 2,
		            "index out of range.\n");
		return NULL;
	}

	return remove_child (fragment, position, status);
}

/*@null@*/
/*@only@*/
struct SH_Text *
SH_NodeFragment_to_html (const struct SH_NodeFragment * fragment,
                         enum HTML_MODE mode,
                         unsigned int indent_base,
                         unsigned int indent_step,
                         const char * indent_char,
                         /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	struct SH_Text * html;
	struct SH_Text * child;
	struct SH_Text * indent_text;
	size_t index;

	html = SH_Text_new (status);
	if (html == NULL)
	{
		goto fail;
	}

	if (mode == WRAP)
	{
		indent_text = SH_Text_new (status);
		if (indent_text == NULL)
		{
			goto fail_clean_html;
		}

		for (index = 0; ((unsigned int) index) < indent_base;
		     index++)
		{
			if (!SH_Text_append_string (indent_text,
			                            indent_char,
			                            status))
			{
				goto fail_clean_indent;
			}
		}

		if (!SH_Text_append_text (html, indent_text, status))
		{
			goto fail_clean_indent;
		}
	}

	if (!SH_Text_append_string (html, OPEN_TAG_BEGIN, status))
	{
		goto fail_autoclean;
	}

	if (!SH_Text_append_string (html, fragment->tag, status))
	{
		goto fail_autoclean;
	}

	for (index = 0; index < fragment->attr_n; index++)
	{
	#define add(TEXT) SH_Text_append_string (html, (TEXT), status)
	#define attr &(fragment->attrs[index])

		if (!add (ATTR_NAME_DELIMITER))
		{
			goto fail_autoclean;
		}

		if (!add (Attr_get_name (attr)))
		{
			goto fail_autoclean;
		}

		if (Attr_has_value (attr))
		{
			if (!add (ATTR_VALUE_DELIMITER))
			{
				goto fail_autoclean;
			}

			if (!add (ATTR_VALUE_QUOTE_BEGIN))
			{
				goto fail_autoclean;
			}

			if (!add (Attr_get_value (attr)))
			{
				goto fail_autoclean;
			}

			if (!add (ATTR_VALUE_QUOTE_END))
			{
				goto fail_autoclean;
			}
		}
	#undef attr
	#undef add
	}

	if (!SH_Text_append_string (html, OPEN_TAG_END, status))
	{
		goto fail_autoclean;
	}

	if (mode == WRAP)
	{
		if (!SH_Text_append_string (html, NEWLINE, status))
		{
			goto fail_clean_indent;
		}
	}

	for (index = 0; index < fragment->child_n; index++)
	{
		child = SH_Fragment_to_html (fragment->childs[index],
					     mode,
					     indent_base + indent_step,
					     indent_step,
					     indent_char,
					     status);

		if (child == NULL)
		{
			goto fail_autoclean;
		}

		SH_Text_join (html, child);
	}

	if (mode == WRAP)
	{
		if (!SH_Text_append_text (html, indent_text, status))
		{
			goto fail_clean_indent;
		}
	}

	if (!SH_Text_append_string (html, CLOSE_TAG_BEGIN, status))
	{
		goto fail_autoclean;
	}

	if (!SH_Text_append_string (html, fragment->tag, status))
	{
		goto fail_autoclean;
	}

	if (!SH_Text_append_string (html, CLOSE_TAG_END, status))
	{
		goto fail_autoclean;
	}

	if (mode == WRAP)
	{
		if (!SH_Text_append_string (html, NEWLINE, status))
		{
			goto fail_clean_indent;
		}

		SH_Text_free (indent_text);
	}

	set_success (status);

	return html;

fail_autoclean:
	if (mode == WRAP) goto fail_clean_indent;
	else goto fail_clean_html;

fail_clean_indent:
	SH_Text_free (indent_text);
fail_clean_html:
	SH_Text_free (html);
fail:
	return NULL;
}
