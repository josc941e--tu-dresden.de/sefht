/*
 * fragment.h
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef SEFHT_FRAGMENT_H
#define SEFHT_FRAGMENT_H

#if !defined (SEFHT_SEFHT_H_INSIDE) && !defined (SEFHT_COMPILATION)
#error "Only <sefht/sefht.h> can be included directly."
#endif

#include "status.h"

#include "data.h"
#include "text.h"


enum HTML_MODE
{
	INLINE,
	WRAP
};

typedef /*@abstract@*/ struct SH_Fragment SH_Fragment;


/*@null@*/
/*@only@*/
struct SH_Fragment *
SH_Fragment_copy (const SH_Fragment * fragment,
                  /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

void
SH_Fragment_free (/*@only@*/ SH_Fragment * fragment)
	/*@modifies fragment@*/
	/*@releases fragment@*/;

bool
SH_Fragment_is_orphan (const SH_Fragment * fragment)
	/*@*/;

/*@null@*/
/*@only@*/
SH_Text *
SH_Fragment_to_html (const struct SH_Fragment * fragment,
                     enum HTML_MODE mode,
                     unsigned int indent_base,
                     unsigned int indent_step,
                     const char * indent_char,
                     /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

#endif /* SEFHT_FRAGMENT_H */
