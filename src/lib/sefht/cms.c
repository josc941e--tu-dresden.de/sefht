/*
 * cms.c
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <stdlib.h>

#include "macro.h"
#include "log.h"
#include "status.h"

#include "data.h"

#include "cms.h"


struct SH_Cms
{
	/*@only@*/ SH_Data * data;
};


/*@null@*/
/*@only@*/
struct SH_Cms *
SH_Cms_new (/*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	struct SH_Cms * cms;
	cms = malloc (sizeof (struct SH_Cms));

	if (cms == NULL)
	{
		set_status (status, E_ALLOC, 4,
		            "Memory allocation for SH_Cms failed.\n");

		return NULL;
	}

	cms->data = SH_Data_new (status);

	if (cms->data == NULL)
	{
		free (cms);
		return NULL;
	}

	set_success (status);
	return cms;
}

void
SH_Cms_free (/*@only@*/ struct SH_Cms * cms)
	/*@modifies cms->data@*/
	/*@modifies cms@*/
	/*@releases cms@*/
{
	SH_Data_free (cms->data);

	free (cms);

	return;
}

page_t
SH_Cms_register_page (struct SH_Cms * cms, const char * name,
                      struct SH_Status * status)
	/*@modifies cms->data@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	return SH_Data_register_page (cms->data, name, status);
}
