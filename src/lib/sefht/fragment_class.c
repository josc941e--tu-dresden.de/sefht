/*
 * fragment_class.c
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef SEFHT_FRAGMENT_CLASS_C
#define SEFHT_FRAGMENT_CLASS_C

#if !defined (SEFHT_SEFHT_H_INSIDE) && !defined (SEFHT_COMPILATION)
#error "Only <sefht/sefht.h> can be included directly."
#endif

#include "status.h"

#include "data.h"
#include "text.h"


#ifdef SEFHT_COMPILATION
#include "fragment_data.c"

static inline
void
init_fragment (/*@out@*/ struct SH_Fragment * fragment,
               const enum SH_FRAGMENT_TYPE type,
               /*@shared@*/
               const struct fragment_methods * const methods,
               /*@dependent@*/ SH_Data * data,
               /*@null@*/ /*@dependent@*/
               struct SH_NodeFragment * parent)
	/*@modifies fragment->type@*/
	/*@modifies fragment->methods@*/
	/*@modifies fragment->data@*/
	/*@modifies fragment->parent@*/
{
	fragment->type = type;
	fragment->methods = methods;

	fragment->data = data;

	fragment->parent = parent;

	return;
}

static inline
void
free_fragment (struct SH_Fragment * fragment)
	/*@modifies fragment@*/
	/*@releases fragment@*/
{
	(void) fragment;
	return;
}

static inline
void
copy_fragment (/*@out@*/ struct SH_Fragment * copy,
               const struct SH_Fragment * fragment)
	/*@modifies copy->type@*/
	/*@modifies copy->methods@*/
	/*@modifies copy->data@*/
	/*@modifies copy->parent@*/
{
	copy->type = fragment->type;
	copy->methods = fragment->methods;

	copy->data = fragment->data;

	copy->parent = NULL;

	return;
}

static inline
enum SH_FRAGMENT_TYPE
get_type (const struct SH_Fragment * fragment)
	/*@*/
{
	return fragment->type;
}

static inline
/*@null@*/
/*@dependent@*/
struct SH_NodeFragment *
get_parent (const struct SH_Fragment * fragment)
{
	return fragment->parent;
}

static inline
void
set_parent (struct SH_Fragment * fragment,
            /*@null@*/ /*@dependent@*/ struct SH_NodeFragment * parent)
	/*@modifies fragment->parent@*/
{
	fragment->parent = parent;
	return;
}

#endif /* SEFHT_COMPILATION */
#endif /* SEFHT_FRAGMENT_CLASS_C */
