/*
 * text.h
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef SEFHT_TEXT_H
#define SEFHT_TEXT_H

#if !defined (SEFHT_SEFHT_H_INSIDE) && !defined (SEFHT_COMPILATION)
#error "Only <sefht/sefht.h> can be included directly."
#endif

#include <stdbool.h>

#include "status.h"


typedef /*@abstract@*/ struct SH_Text SH_Text;


/*@null@*/
/*@only@*/
SH_Text *
SH_Text_new (/*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

/*@null@*/
/*@only@*/
SH_Text *
SH_Text_new_from_string (const char * string,
                         /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

void
SH_Text_free (/*@only@*/ SH_Text * text)
	/*@modifies text@*/
	/*@releases text@*/;

/*@null@*/
/*@only@*/
SH_Text *
SH_Text_copy (const SH_Text * text,
              /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

size_t
SH_Text_get_length (const struct SH_Text * text,
                    /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

/*@null@*/
/*@only@*/
char *
SH_Text_get_char (const SH_Text * text,
                  size_t index,
                  /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

/*@null@*/
/*@only@*/
char *
SH_Text_get_string (const SH_Text * text,
                    size_t index, size_t offset,
                    /*@null@*/ /*@out@*/ size_t * length,
                    /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@modifies length@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

/*@null@*/
/*@only@*/
char *
SH_Text_get_range (const SH_Text * text,
                   size_t start, size_t end,
                   /*@null@*/ /*@out@*/ size_t * length,
                   /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@modifies length@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_Text_set_char (SH_Text * text, size_t index,
                  const char character,
                  /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@modifies text@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_Text_append_string (SH_Text * text, const char * string,
                       /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@modifies text@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_Text_append_text (SH_Text * text,
                     const SH_Text * text2,
                     /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@modifies text@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

void
SH_Text_join (SH_Text * text,
              /*@only@*/ SH_Text * text2)
	/*@modifies text@*/
	/*@modifies text2@*/
	/*@releases text2@*/;

void
SH_Text_print (const SH_Text * text)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/;

#endif /* SEFHT_TEXT_H */
