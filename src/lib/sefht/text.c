/*
 * text.c
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <errno.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "macro.h"
#include "log.h"
#include "status.h"

#include "text.h"


#ifndef CHUNK_SIZE
#define CHUNK_SIZE 64
#endif /* CHUNK_SIZE */

#if CHUNK_SIZE == 0
#error "CHUNK_SIZE can't be 0."
#endif /* CHUNK_SIZE == 0 */

struct text_segment;
struct text_segment
{
	size_t length;
	size_t size;
	char * text;
	/*@relnull@*/ struct text_segment * next;
};

struct SH_Text
{
	/*@notnull@*/ struct text_segment * data;
};


/*@null@*/
/*@only@*/
struct SH_Text *
SH_Text_new (/*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	struct SH_Text * text;
	text = malloc (sizeof (struct SH_Text));

	if (text == NULL)
	{
		set_status (status, E_ALLOC, 4,
		            "Memory allocation for SH_Text failed.\n");

		return NULL;
	}

	text->data = malloc (sizeof (struct text_segment));

	if (text->data == NULL)
	{
		set_status (status, E_ALLOC, 4,
		            "Memory allocation for SH_Text failed.\n");

		free (text);

		return NULL;
	}

	text->data->length = 0;
	text->data->text = malloc (CHUNK_SIZE * sizeof (char));

	if (text->data->text == NULL)
	{
		set_status (status, E_ALLOC, 4,
		            "Memory allocation for SH_Text failed.\n");

		free (text->data);
		free (text);

		return NULL;
	}

	text->data->text[0] = (char) 0;
	text->data->size = (size_t) CHUNK_SIZE;
	text->data->next = NULL;

	set_success (status);

	return text;
}

/*@null@*/
/*@only@*/
struct SH_Text *
SH_Text_new_from_string (const char * string,
                         /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	struct SH_Text * text;
	text = malloc (sizeof (struct SH_Text));

	if (text == NULL)
	{
		set_status (status, E_ALLOC, 4,
		            "Memory allocation for SH_Text failed.\n");

		return NULL;
	}

	text->data = malloc (sizeof (struct text_segment));

	if (text->data == NULL)
	{
		set_status (status, E_ALLOC, 4,
		            "Memory allocation for SH_Text failed.\n");

		free (text);

		return NULL;
	}

	text->data->length = strlen (string);

	if (text->data->length == SIZE_MAX)
	{
		set_status (status, E_DOMAIN, 2,
		            "Maximum length of SH_Text reached.\n");

		free (text->data);
		free (text);

		return NULL;
	}

	text->data->text = strdup (string);

	if (text->data->text == NULL)
	{
		set_status (status, E_ALLOC, 4,
		            "Memory allocation for SH_Text failed.\n");

		free (text->data);
		free (text);

		return NULL;
	}

	text->data->size = text->data->length + 1;
	text->data->next = NULL;

	set_success (status);

	return text;
}

void
SH_Text_free (/*@only@*/ struct SH_Text * text)
	/*@modifies text@*/
	/*@releases text@*/
{
	struct text_segment * next;

	for (next = text->data; next != NULL; text->data = next)
	{
		next = next->next;

		free (text->data->text);
		free (text->data);
	}

	free (text);

	return;
}

/*@null@*/
/*@only@*/
struct SH_Text *
SH_Text_copy (const struct SH_Text * text,
              /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	const struct text_segment * start;
	const struct text_segment * end;
	struct text_segment * copy_seg;

	struct SH_Text * copy;
	copy = malloc (sizeof (struct SH_Text));

	if (copy == NULL)
	{
		set_status (status, E_ALLOC, 4,
		            "Memory allocation for SH_Text failed.\n");

		return NULL;
	}

	copy->data = malloc (sizeof (struct text_segment));

	if (copy->data == NULL)
	{
		set_status (status, E_ALLOC, 4,
		            "Memory allocation for SH_Text failed.\n");

		free (copy);

		return NULL;
	}

	copy->data->next = NULL;

	/* copy_seg is the segment we're currently copying to */
	copy_seg = copy->data;

	/* start to end are the segments we're copying from. */
	/* start points to the first segment to copy.
	 * end points to the first segment not being copied in
	 * the current iteration. */
	end = text->data;
	start = end;

	/* At the beginning of each iteration start == end.
	 * This is ensured before the loop and at the end of the outer
	 * loop with the last for loop, which does the actual copying.
	 *
	 * Then end is adjusted to point after the last segment to be
	 * copied. If it isn't NULL afterwards another loop iteration
	 * has to take place.
	 *
	 * With start and end properly set, and the length and size of
	 * the resulting segment determined, the copying can start,
	 * which is just a repeated string concatenation.
	 *
	 * If another iteration is following, the next segment has to be
	 * allocated. This can't be done at the beginning, because
	 * before the first iteration the pointer to the allocated
	 * segment must be written to copy->data directly to create the
	 * anchor of the linked list. (So there is no ?->next
	 * to write to.)
	 * Actually copy_seg can also be set to &(copy->data) before
	 * the loop and to &((*copy_seg)->next) after each iteration,
	 * but that would be complicate things...
	 * Implementing the loop as for loop and writing the allocation
	 * code into their head is also not possible, because compound
	 * statements aren't allowed there.
	 * Thus the allocating has to take place at the end of the loop
	 * (if necessary) and the strange if(end==NULL){break;} has to
	 * stay there to. */
	/* Actually while (TRUE) would also work, because the loop is
	 * always aborted with break.
	 * (See comment above, last paragraph.) */
	while (end != NULL)
	{
		/* Adjusting end and determine length of the resulting
		 * segment. Actually the end is determined by the
		 * length, because the segment can't be greather than
		 * numbers in size_t. Otherwise we can't address
		 * everything nor save the length. */
		for (copy_seg->length = 0; end != NULL; end = end->next)
		{
			/* Stop moving the end further then what we
			 * can address. Actually copy_seg->length could
			 * be one greater, so the comparison could be
			 * with >=, but copy_seg->size should also be
			 * expressible and this is always at least by
			 * one greater, because of the terminating
			 * NULL-byte. */
			if (SIZE_MAX - end->length > copy_seg->length)
			{
				copy_seg->length += end->length;
			}
			/* This can't be moved after the loop, because,
			 * if the loop exits normally, this must not be
			 * executed. */
			else
			{
				end = end->next;

				/*@innerbreak@*/
				break;
			}
		}

		/* This addition can not overflow, because in the
		 * for-loop it was tested with > instead of >= which
		 * would be appropriate. See comment there. */
		copy_seg->size = copy_seg->length + 1;

		copy_seg->text = malloc (copy_seg->size * sizeof (char));

		if (copy_seg->text == NULL)
		{
			set_status (status, E_ALLOC, 4,
			            "Memory allocation for SH_Text "
			            "failed.\n");

			for (copy_seg = copy->data->next;
			     copy_seg != NULL;
			     copy->data = copy_seg,
			     copy_seg = copy_seg->next)
			{
				if (copy->data->text != NULL)
				{
					free (copy->data->text);
				}

				free (copy->data);
			}
			free (copy->data);

			free (copy);

			return NULL;
		}

		copy_seg->text[0] = (char) 0;

		/* actual copying */
		/* Initialization was done since the beginning of
		 * the outer loop. So think of all this code standing
		 *   |  here.
		 *   v        */
		for (; start != end; start = start->next)
		{
			strcat (copy_seg->text, start->text);
		}

		/* See comment of outer loop. (3rd paragraph) */
		if (end == NULL)
		{
			break;
		}

		copy_seg->next = malloc (sizeof (struct text_segment));

		if (copy_seg->next == NULL)
		{
			set_status (status, E_ALLOC, 4,
			            "Memory allocation for SH_Text "
			            "failed.\n");

			for (copy_seg = copy->data->next;
			     copy_seg != NULL;
			     copy->data = copy_seg,
			     copy_seg = copy_seg->next)
			{
				if (copy->data->text != NULL)
				{
					free (copy->data->text);
				}

				free (copy->data);
			}
			free (copy->data);

			free (copy);

			return NULL;
		}

		copy_seg = copy_seg->next;
		copy_seg->next = NULL;
	}

	/* This could be also moved inside the loop instead of the
	 * break, because the loop quits always by this break.
	 * Also see the comment of the outer loop above. */
	set_success (status);

	return copy;
}

size_t
SH_Text_get_length (const struct SH_Text * text,
                    /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	size_t length;
	const struct text_segment * seg;

	length = 0;
	for (seg = text->data; seg != NULL; seg = seg->next)
	{
		if (SIZE_MAX - seg->length >= length)
		{
			length += seg->length;
		}
		else
		{
			set_status (status, E_DOMAIN, 6,
			            "SH_Text: length >= SIZE_MAX\n");

			return SIZE_MAX;
		}
	}

	set_success (status);

	return length;
}

/*@null@*/
/*@only@*/
char *
SH_Text_get_char (const struct SH_Text * text,
                  size_t index,
                  /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	char * character;
	const struct text_segment * seg;

	/* find seg, where the index points to OR set seg to NULL */
	for (seg = text->data; (seg != NULL) && (index >= seg->length);
	     index -= seg->length, seg = seg->next);

	if (seg == NULL)
	{
		set_status (status, E_VALUE, 2,
		            "SH_Text: index out of range.\n");

		return NULL;
	}

	/* allocate char to be returned */
	character = malloc (sizeof (char));

	if (character == NULL)
	{
		set_status (status, E_ALLOC, 4,
		            "Memory allocation for SH_Text failed.\n");

		return NULL;
	}

	/* set char */
	*character = seg->text[index];

	set_success (status);

	return character;
}

/*@null@*/
/*@only@*/
char *
SH_Text_get_string (const struct SH_Text * text,
                    size_t index, size_t offset,
                    /*@null@*/ /*@out@*/ size_t * length,
                    /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@modifies length@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	size_t len;
	char * string;
	const struct text_segment * start;
	const struct text_segment * end;

	/* Determine the begin of the requested string.
	 * start: The segment where the requested string starts.
	 * index: The index of the first char relative to
	          the first segment. */
	for (start = text->data;
	     (start != NULL) && (index >= start->length);
	     start = start->next)
	{
		/* The subtraction is save, because this is,
		 * what is tested for in the condition. */
		index -= start->length;
	}

	/* The end of text was encountered while looking for
	 * the start segment. Those the index is out of range. */
	if (start == NULL)
	{
		set_status (status, E_VALUE, 2,
		            "SH_Text: index out of range.\n");

		if (length != NULL)
		{
			*length = 0;
		}

		return NULL;
	}

	/* The requested string is fully inside one segment.
	 * So set end and len offset appropriately.
	 * Note that offset isn't relative to the segment,
	 * but to index, but this is what we need, since we
	 * want to copy relative to index. */
	if ((offset < SIZE_MAX - index)
	&& (index + offset <= start->length))
	{
		end = start;
		len = offset;
	}

	/* The requested string is in more than one segment.
	 * Find the segment of the text, where the last part of
	 * the string is contained and set offset relative to it.
	 * end: The segment where the last part of the string
	 *      is located.
	 * offset: The remaining length of the string inside
	 *         this last segment.
	 *
	 * Also the absolute length is needed for allocating the string
	 * and returning it. This would be the original value of offset,
	 * but it can not be set directly, because offset could be
	 * longer then the text actually is. Those both has to be set
	 * in each iteration over the text, till the last segment
	 * is encountered (the offset is smaller then the length of
	 * the segment). */
	else
	{
		/* skip the remaining of the start segment */
		/* The subtraction is save, because index is relative
		 * to the start segment (smaller then start->length). */
		len = start->length - index;
		/* The subtraction is save, because this is the
		 * else branch. (offset + index > start->length) */
		offset -= len;

		/* Iterate over the text till the offset is inside
		 * the current segment. Actually the comparison has
		 * to be made against end->next, because, if offset
		 * points outside of the text, a pointer to the last
		 * segment has to be preserved. Otherwise we would
		 * have to iterate over the text another time. */
		for (end = start;
		     (end->next != NULL) && (offset > end->next->length);
		     end = end->next)
		{
			/* The subtraction is save because this is,
			 * what is tested for.
			 * The addition is save, because len will be
			 * equal or smaller then the original offset
			 * and this is also of size_t. Note that
			 * len = offset;
			 * is not possible in the first place,
			 * see the comment before the else branch. */
			len += end->next->length;
			offset -= end->next->length;
		}

		/* We have encountered the end of text. Set offset to
		 * the length of the last segment (making it smaller);
		 * end already points to the last (non-NULL) segment. */
		if (end->next == NULL)
		{
			offset = end->length;
		}
		/* The end of the requested string is inside the
		 * next segment. The variable offset is already
		 * relative to it, so just add it to the length.
		 * Also set end to the next segment, this can't
		 * be done earlier, because it may be NULL.
		 * (See comment before if.) */
		else
		{
			/* The addition is save, because len will be
			 * equal or smaller then the original offset
			 * and this is also of size_t.
			 * Note that len = offset is not possible in
			 * the first place, see the comment before
			 * the outer else branch. */
			len += offset;
			end = end->next;
		}
	}

	/* Allocate the string to be returned. The multiplication is
	 * save, as long is sizeof (char) == 1. (Should be ...) */
	string = malloc ((len + 1) * sizeof (char));

	/* Allocating has failed. Nothing has to be freed, because we
	 * just have some stack vars and pointers to segments. */
	if (string == NULL)
	{
		set_status (status, E_ALLOC, 6,
		            "Memory allocation for SH_Text failed.\n");

		if (length != NULL)
		{
			*length = 0;
		}

		return NULL;
	}

	/* add terminating NULL byte. */
	string[0] = (char) 0;

	/* The requested string is fully inside one segment. */
	if (start == end)
	{
		strncat (string, start->text + index, offset);
	}
	/* The requested string is in more than one segment. */
	else
	{
		/* copy part of the start segment */
		strcat (string, start->text + index);

		/* copy all segments, that are contained in the
		 * requested string fully */
		for (start = start->next; start != end;
		     start = start->next)
		{
			strcat (string, start->text);
		}

		/* copy part of the last segment */
		strncat (string, end->text, offset);
	}


	if (length != NULL)
	{
		*length = len;
	}

	set_success (status);

	return string;
}

/*@null@*/
/*@only@*/
char *
SH_Text_get_range (const struct SH_Text * text,
                   size_t start, size_t end,
                   /*@null@*/ /*@out@*/ size_t * length,
                   /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@modifies length@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	size_t len;
	char * string;
	const struct text_segment * start_seg;
	const struct text_segment * end_seg;

	/* Determine the begin of the requested string.
	 * start_seg: The segment where the requested string starts.
	 * start: The index of the first char relative to
	 *        the first segment.
	 * end: The index after the last char relative to
	 *      the first segment */
	for (start_seg = text->data;
	     (start_seg != NULL) && (start >= start_seg->length);
	     start_seg = start_seg->next)
	{
		/* The subtraction is save, because this is,
		 * what is tested for in the condition. */
		start -= start_seg->length;

		/* The variable end has also to be adjusted, because
		 * otherwise we would have to start iterating at
		 * the beginning again.
		 * So this way it is more efficiently.
		 *
		 * The subtraction is save, because the end is always
		 * greater than start and the last was save also. */
		end -= start_seg->length;
	}

	/* The end of text was encountered while looking for
	 * the start segment. Those the index is out of range. */
	if (start_seg == NULL)
	{
		set_status (status, E_VALUE, 2,
		            "SH_Text: index out of range.\n");

		if (length != NULL)
		{
			*length = 0;
		}

		return NULL;
	}

	/* The requested string is fully inside one segment.
	 * So set end_seg and len appropriately; end is already
	 * relative to the first segment. */
	if (end <= start_seg->length)
	{
		end_seg = start_seg;
		len = end - start;
	}

	/* The requested string is in more than one segment.
	 * Find the segment of the text, where the last part of
	 * the string is contained and set end relative to it.
	 * end_seg: The segment where the last part of the string
	 *          is located.
	 * end: The remaining length of the string inside
	 *      this last segment.
	 *
	 * Also the absolute length is needed for allocating the string
	 * and returning it. This would be the original value of
	 * (end - start), but it can not be set directly, because end
	 * could be point outside of the text. Those both has to be set
	 * in each iteration over the text, till the last segment
	 * is encountered (the end is smaller then the length of
	 * the segment). */
	else
	{
		/* skip the remaining of the start segment */
		/* The subtraction is save, because start is relative
		 * to the start segment (start < start_seg->length). */
		len = start_seg->length - start;
		/* The subtraction is save, because this is the
		 * else branch. (end > start->length) */
		end -= start_seg->length;

		/* Iterate over the text till the end is inside
		 * the current segment. Actually the comparison has
		 * to be made against end_seg->next, because, if end
		 * points outside of the text, a pointer to the last
		 * segment has to be preserved. Otherwise we would
		 * have to iterate over the text another time. */
		for (end_seg = start_seg;
		        (end_seg->next != NULL)
		     && (end > end_seg->next->length);
		     end_seg = end_seg->next)
		{
			/* The subtraction is save because this is,
			 * what is tested for.
			 * The addition is save, because len will be
			 * equal or smaller then the original
			 * (end - start)
			 * and they is also of size_t and end > start.
			 * Note that len = offset; is not possible
			 * in the first place,
			 * see the comment before the else branch. */
			len += end_seg->next->length;
			end -= end_seg->next->length;
		}

		/* We have encountered the end of text. Set end to
		 * the length of the last segment (making it smaller);
		 * end_seg already points to the last (non-NULL)
		 * segment. */
		if (end_seg->next == NULL)
		{
			end = end_seg->length;
		}
		/* The end of the requested string is inside the
		 * next segment. The variable end is already
		 * relative to it, so just add it to the length.
		 * Also set end_seg to the next segment, this can't
		 * be done earlier, because it may be NULL.
		 * (See comment before if.) */
		else
		{
			/* The addition is save, because len will be
			 * equal or smaller then the original
			 * (end - start)
			 * and they is also of size_t and end > start.
			 * Note that len = offset is not possible in
			 * the first place, see the comment before
			 * the outer else branch. */
			len += end;
			end_seg = end_seg->next;
		}
	}

	/* Allocate the string to be returned. The multiplication is
	 * save, as long as sizeof (char) == 1. (Should be ...) */
	string = malloc ((len + 1) * sizeof (char));

	/* Allocating has failed. Nothing has to be freed, because we
	 * just have some stack vars and pointers to segments. */
	if (string == NULL)
	{
		set_status (status, E_ALLOC, 6,
		            "Memory allocation for SH_Text failed.\n");

		if (length != NULL)
		{
			*length = 0;
		}

		return NULL;
	}


	/* add terminating NULL byte. */
	string[0] = (char) 0;

	/* The requested string is fully inside one segment. */
	if (start_seg == end_seg)
	{
		strncat (string, start_seg->text + start, len);
	}
	/* The requested string is in more than one segment. */
	else
	{
		/* copy part of the start segment */
		strcat (string, start_seg->text + start);

		/* copy all segments, that are contained in the
		 * requested string fully */
		for (start_seg = start_seg->next; start_seg != end_seg;
		     start_seg = start_seg->next)
		{
			strcat (string, start_seg->text);
		}

		/* copy part of the last segment */
		strncat (string, end_seg->text, end);
	}


	if (length != NULL)
	{
		*length = len;
	}

	set_success (status);

	return string;
}

bool
SH_Text_set_char (struct SH_Text * text, size_t index,
                  const char character,
                  /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@modifies text->data@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	const struct text_segment * seg;

	/* find the segment, where the character should be written to.*/
	for (seg = text->data; (seg != NULL) && (index >= seg->length);
	     seg = seg->next)
	{
		/* The subtraction is save, because this is,
		 * what is tested for in the condition. */
		index -= seg->length;
	}


	/* The end of text was encountered while looking for
	 * the segment. Those the index is out of range. */
	if (seg == NULL)
	{
		set_status (status, E_VALUE, 13, "index out of range\n");
		return FALSE;
	}

	/* set character */
	seg->text[index] = character;

	set_success (status);
	return TRUE;
}

bool
SH_Text_append_string (struct SH_Text * text, const char * string,
                       /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@modifies text->data@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	size_t length;
	size_t size;
	char * new_text;
	struct text_segment * seg;

	length = strlen (string);

	/* Fiddling around with more than one segment
	 * would be to cumbersome. */
	if (length == SIZE_MAX)
	{
		set_status (status, E_DOMAIN, 2,
		            "Maximum length of SH_Text reached.\n");

		return FALSE;
	}

	/* iterate to last segment
	 * This can't be done in the end, because it is tried
	 * to write the string to the last segment, if there
	 * is still enough space. */
	for (seg = text->data; seg->next != NULL; seg = seg->next);

	/* The string fits at the end of the last segment. */
	if (SIZE_MAX - seg->length > length)
	{
		length += seg->length;
	}
	/* It doesn't, so create a new segment after it. */
	else
	{
		seg->next = malloc (sizeof (struct text_segment));

		if (seg->next == NULL)
		{
			set_status (status, E_ALLOC, 4,
			            "Memory allocation for "
			            "SH_Text data failed.\n");

/* useless assignment to silence splint */
#ifdef S_SPLINT_S
			seg->next = NULL;
#endif

			return FALSE;
		}
	}


	/* Figure out how much memory has to be allocated
	 * in multiplies of chunk size. If this would overflow,
	 * just take the really needed size. */
	/* If both operands were integers, this operation would fail
	 * as the division will behave, like calling floor
	 * in the first place. */
	/* length + 1 is save, because at the last if
	 * we tested for > instead of >= */
	size = (size_t) ceilf ((float) (length+1) / (float) CHUNK_SIZE);
	if (size > (SIZE_MAX / (float) CHUNK_SIZE))
	{
		size = length + 1;
	}
	else
	{
		size *= CHUNK_SIZE;
	}

	/* Enlarge the text. */
	if (seg->next == NULL)
	{
		new_text = realloc (seg->text, size * sizeof (char));
	}
	/* If a new segment is made, there is also a new text */
	else
	{
		new_text = malloc (size * sizeof (char));
	}

	/* Allocation failed */
	if (new_text == NULL)
	{
		set_status (status, E_ALLOC,
		            (seg->next == NULL) ? 11: 6,
		            "Memory allocation for "
		            "SH_Text data failed.\n");

		/* Clean-up, if new segment was to be made. */
		if (seg->next != NULL)
		{
			free (seg->next);
			seg->next = NULL;
		}

/* useless assignment to silence splint */
#ifdef S_SPLINT_S
			seg->next = NULL;
#endif

		return FALSE;
	}

	/* If a new text is made, add the terminating NULL to
	 * the string and switch to the new segment. */
	if (seg->next != NULL)
	{
		new_text[0] = (char) 0;

		seg = seg->next;
		seg->next = NULL;
	}

	/* Save string and write changes */
	seg->text = strcat (new_text, string);
	seg->length = length;
	seg->size = size;

	set_success (status);

	return TRUE;
}

bool
SH_Text_append_text (struct SH_Text * text,
                     const struct SH_Text * text2,
                     /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@modifies text->data@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	struct SH_Text * copy;
	struct text_segment * seg;

	copy = SH_Text_copy (text2, status);

	if (copy == NULL)
	{
		return FALSE;
	}

	/* go to last segment */
	for (seg = text->data; seg->next != NULL; seg = seg->next);

	/* append */
	seg->next = copy->data;

	free (copy);

	return TRUE;
}

void
SH_Text_join (struct SH_Text * text,
              /*@only@*/ struct SH_Text * text2)
	/*@modifies text->data@*/
	/*@modifies text2@*/
	/*@releases text2@*/
{
	struct text_segment * seg;

	/* go to last segment */
	for (seg = text->data; seg->next != NULL; seg = seg->next);

	/* append contents */
	seg->next = text2->data;

	/* destroy text2 */
	free (text2);

	return;
}

void
SH_Text_print (const struct SH_Text * text)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
{
	const struct text_segment * seg;

	for (seg = text->data; seg != NULL; seg = seg->next)
	{
		printf ("%s", seg->text);
	}

	return;
}
