/*
 * text_fragment.c
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <stdlib.h>


#include "macro.h"
#include "log.h"
#include "status.h"

#include "text.h"

#include "fragment.h"

#include "text_fragment.h"


#include "fragment_class.c"


struct SH_TextFragment
{
	struct SH_Fragment base;

	/*@only@*/ struct SH_Text * text;
};


static const struct fragment_methods methods = {
	(struct SH_Fragment * (*)(const struct SH_Fragment *,
	                          /*@out@*/ /*@null@*/
	                          struct SH_Status *))
	SH_TextFragment_copy,

	(void (*)(/*@only@*/ struct SH_Fragment *))
	SH_TextFragment_free,

	(struct SH_Text * (*)(const struct SH_Fragment *,
	                      enum HTML_MODE,
	                      unsigned int, unsigned int, const char *,
	                      /*@out@*/ /*@null@*/ struct SH_Status *))
	SH_TextFragment_to_html
};


/*@null@*/
/*@only@*/
struct SH_Fragment /*@alt struct SH_TextFragment*/ *
SH_TextFragment_new (/*@dependent@*/ SH_Data * data,
                     /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@globals TEXT,
	           fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	struct SH_TextFragment * fragment;

	fragment = malloc (sizeof (struct SH_TextFragment));
	if (NULL == fragment)
	{
		set_status (status, E_ALLOC, 3, "malloc failed");
		return NULL;
	}

	init_fragment (&(fragment->base), TEXT, &methods, data, NULL);

	fragment->text = SH_Text_new (status);
	if (NULL == fragment->text)
	{
		free (fragment);
		return NULL;
	}

	set_success (status);
	return (struct SH_Fragment *) fragment;
}

void
SH_TextFragment_free (/*@only@*/ struct SH_TextFragment * fragment)
	/*@modifies fragment@*/
	/*@releases fragment@*/
{
	SH_Text_free (fragment->text);

	free_fragment (&fragment->base);

	free (fragment);
	return;
}

/*@null@*/
/*@only@*/
struct SH_Fragment /*@alt struct SH_TextFragment@*/ *
SH_TextFragment_copy (const struct SH_TextFragment * fragment,
                      /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	struct SH_TextFragment * copy;

	copy = malloc (sizeof (struct SH_TextFragment));
	if (NULL == copy)
	{
		set_status (status, E_ALLOC, 3, "malloc failed");
		return NULL;
	}

	copy_fragment (&(copy->base), &(fragment->base));

	copy->text = SH_Text_copy (fragment->text, status);
	if (NULL == fragment->text)
	{
		free (copy);
		return NULL;
	}

	set_success (status);
	return (struct SH_Fragment *) copy;
}

/*@observer@*/
const struct SH_Text *
SH_TextFragment_get_text (const struct SH_TextFragment * fragment)
	/*@*/
{
	return fragment->text;
}

bool
SH_TextFragment_append_string (struct SH_TextFragment * fragment,
                               const char * string,
                               /*@null@*/ /*@out@*/
                               struct SH_Status * status)
	/*@modifies fragment->text@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	return SH_Text_append_string (fragment->text, string, status);
}

bool
SH_TextFragment_append_text (struct SH_TextFragment * fragment,
                             const SH_Text * text,
                             /*@null@*/ /*@out@*/
                             struct SH_Status * status)
	/*@modifies fragment->text@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	return SH_Text_append_text (fragment->text, text, status);
}

void
SH_TextFragment_join (struct SH_TextFragment * fragment,
                      /*@only@*/ SH_Text * text)
	/*@modifies fragment->text@*/
	/*@modifies text@*/
	/*@releases text@*/
{
	return SH_Text_join (fragment->text, text);
}

void
SH_TextFragment_print (const struct SH_TextFragment * fragment)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
{
	return SH_Text_print (fragment->text);
}

/*@null@*/
/*@only@*/
struct SH_Text *
SH_TextFragment_to_html (struct SH_TextFragment * fragment,
                         enum HTML_MODE mode,
                         unsigned int indent_base,
                         unsigned int indent_step,
                         char * indent_char,
                         /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	(void)mode;
	(void)indent_base;
	(void)indent_step;
	(void)indent_char;

	return SH_Text_copy (fragment->text, status);
}
