/*
 * status.h
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef SEFHT_STATUS_H
#define SEFHT_STATUS_H

#include <errno.h>
#include <string.h>

#include "log.h"
#include "macro.h"

#if !defined (SEFHT_SEFHT_H_INSIDE) && !defined (SEFHT_COMPILATION)
#error "Only <sefht/sefht.h> can be included directly."
#endif


#ifdef SEFHT_COMPILATION

#define succeed(STATUS)                                                \
	(((STATUS) != NULL) && ((STATUS)->status == SUCCESS))

#define failed(STATUS)                                                 \
	(((STATUS) != NULL) && ((STATUS)->status != SUCCESS))

#define unknown(STATUS) ((STATUS) == NULL)

/*@notfunction@*/
#define set_status(STATUS, ERROR, OFFSET, MESSAGE)                     \
do                                                                     \
{                                                                      \
	if ((STATUS) != NULL)                                          \
	{                                                              \
		(STATUS)->status = (ERROR);                            \
		(STATUS)->errno_ = ((ERROR) == SUCCESS) ? 0: errno;    \
		(STATUS)->file = (__FILE__);                           \
		(STATUS)->function = (__FUNCTION__);                   \
		(STATUS)->line = (unsigned long) ((__LINE__)-(OFFSET));\
		(STATUS)->message = (MESSAGE);                         \
	};                                                             \
                                                                       \
	if ((MESSAGE) != NULL)                                         \
	{                                                              \
		ERROR1 ((MESSAGE));                                    \
	}                                                              \
}                                                                      \
while (FALSE)

/*@notfunction@*/
#define set_status_(STATUS, ERROR, OFFSET, LENGTH, MESSAGE, ...)       \
do                                                                     \
{                                                                      \
	if ((STATUS) != NULL)                                          \
	{                                                              \
		(STATUS)->status = (ERROR);                            \
		(STATUS)->errno_ = ((ERROR) == SUCCESS) ? 0: errno;    \
		(STATUS)->file = (__FILE__);                           \
		(STATUS)->function = (__FUNCTION__);                   \
		(STATUS)->line = (unsigned long) ((__LINE__)-(OFFSET));\
		(STATUS)->message = malloc(strlen(MESSAGE)+1 + LENGTH);\
		if ((STATUS)->message == NULL)                         \
		{                                                      \
			set_status ((STATUS), E_ALLOC, (OFFSET),       \
			            "malloc failed while generating "  \
			            "error message");                  \
		}                                                      \
		sprintf ((STATUS)->message, (MESSAGE), __VA_ARGS__);   \
	};                                                             \
                                                                       \
	ERROR_ ((MESSAGE), __VA_ARGS__);                               \
}                                                                      \
while (FALSE)

/*@notfunction@*/
#define set_success(STATUS) set_status(STATUS, SUCCESS, 0, NULL)

/*@notfunction@*/
#define _status_preinit(STATUS)                                        \
do {                                                                   \
	(STATUS).status = UNDEFINED;                                   \
	(STATUS).errno_ = 0;                                           \
	(STATUS).file = NULL;                                          \
	(STATUS).line = 0;                                             \
	(STATUS).message = NULL;                                       \
}                                                                      \
while (FALSE)

#endif /* SEFHT_COMPILATION */


struct SH_Status
{
	enum
	{
		UNDEFINED,
		SUCCESS,
		E_ALLOC,
		E_DOMAIN,
		E_VALUE,
		E_STATE,
		E_BUG
	} status;

	int errno_;

	/*@observer@*/ const char * file;
	/*@observer@*/ const char * function;
	unsigned long int line;

	/*@observer@*/ /*@null@*/ char * message;
};

#endif /* SEFHT_STATUS_H */
