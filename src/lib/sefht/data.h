/*
 * data.h
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef SEFHT_DATA_H
#define SEFHT_DATA_H

#if !defined (SEFHT_SEFHT_H_INSIDE) && !defined (SEFHT_COMPILATION)
#error "Only <sefht/sefht.h> can be included directly."
#endif

#include <limits.h>

#include "status.h"

#include "validator.h"


typedef /*@abstract@*/ unsigned int page_t;
#define PAGE_ERR 0
#define PAGE_MIN 1
#define PAGE_MAX UINT_MAX

typedef /*@abstract@*/ struct SH_Data SH_Data;


/*@null@*/
/*@only@*/
SH_Data *
SH_Data_new (/*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

void
SH_Data_free (/*@only@*/ struct SH_Data * data)
	/*@modifies data@*/
	/*@releases data@*/;

page_t
SH_Data_register_page (SH_Data * data, const char * name,
                       struct SH_Status * status)
	/*@modifies data@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

#endif /* SEFHT_DATA_H */
