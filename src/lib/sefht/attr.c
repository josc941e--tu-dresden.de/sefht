/*
 * attr.c
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <stdlib.h>

#include "macro.h"
#include "log.h"
#include "status.h"

#include "attr.h"


#include "attr_static.c"


/*@null@*/
/*@only@*/
struct SH_Attr *
SH_Attr_new (const char * name,
             /*@null@*/ const char * value,
             /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	struct SH_Attr * attr;

	attr = malloc (sizeof (struct SH_Attr));
	if (attr == NULL)
	{
		set_status (status, E_ALLOC, 4, "malloc failed");
		return NULL;
	}

	if (!Attr_init (attr, name, value, status))
	{
		free (attr);
		return NULL;
	}

	set_success (status);
	return attr;
}

/*@null@*/
/*@only@*/
struct SH_Attr *
SH_Attr_raw_new (/*@only@*/ char * name,
                 /*@null@*/ /*@only@*/ char * value,
                 /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	struct SH_Attr * attr;

	attr = malloc (sizeof (struct SH_Attr));
	if (attr == NULL)
	{
		set_status (status, E_ALLOC, 4, "malloc failed");
		return NULL;
	}

	Attr_raw_init (attr, name, value);

	set_success (status);
	return attr;
}

/*@null@*/
/*@only@*/
struct SH_Attr *
SH_Attr_copy (const struct SH_Attr * attr,
             /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	struct SH_Attr * copy;

	copy = malloc (sizeof (struct SH_Attr));
	if (copy == NULL)
	{
		set_status (status, E_ALLOC, 4, "malloc failed");
		return NULL;
	}

	if (!Attr_copy (copy, attr, status))
	{
		free (copy);
		return NULL;
	}

	set_success (status);
	return copy;
}

void
SH_Attr_free (/*@only@*/ struct SH_Attr * attr)
	/*@modifies attr->name@*/
	/*@modifies attr->value@*/
	/*@modifies attr@*/
	/*@releases attr@*/
{
	Attr_free (attr);
	free (attr);
	return;
}

/*@null@*/
/*@only@*/
char *
SH_Attr_get_name (const struct SH_Attr * attr,
                  /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	char * name;

	name = strdup (Attr_get_name (attr));
	if (NULL == name)
	{
		set_status (status, E_ALLOC, 3, "strdup failed");
		return NULL;
	}

	set_success (status);
	return name;
}

bool
SH_Attr_set_name (struct SH_Attr * attr,
                  const char * name,
                  /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@modifies attr->name@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	char * name_copy;

	name_copy = strdup (name);
	if (NULL == name_copy)
	{
		set_status (status, E_ALLOC, 3, "strdup failed");
		return FALSE;
	}

	Attr_set_name (attr, name_copy);

	set_success (status);
	return TRUE;
}

/*@observer@*/
const char *
SH_Attr_raw_get_name (const struct SH_Attr * attr)
	/*@*/
{
	return Attr_get_name (attr);
}

void
SH_Attr_raw_set_name (struct SH_Attr * attr, /*@only@*/ char * name)
	/*@modifies attr->name@*/
{
	Attr_set_name (attr, name);
	return;
}

/*@null@*/
/*@only@*/
char *
SH_Attr_get_value (const struct SH_Attr * attr,
                   /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	char * value;

	if (NULL == Attr_get_value (attr))
	{
		set_success (status);
		return NULL;
	}

	value = strdup (Attr_get_value (attr));
	if (NULL == value)
	{
		set_status (status, E_ALLOC, 3, "strdup failed");
		return NULL;
	}

	set_success (status);
	return value;
}

bool
SH_Attr_set_value (struct SH_Attr * attr,
                   /*@null@*/ const char * value,
                   /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@modifies attr->value@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	char * value_copy;

	if (NULL == value)
	{
		value_copy = NULL;
	}
	else
	{
		value_copy = strdup (value);
		if (NULL == value_copy)
		{
			set_status (status, E_ALLOC, 3, "strdup failed");
			return FALSE;
		}
	}

	Attr_set_value (attr, value_copy);

	set_success (status);
	return TRUE;
}

/*@null@*/
/*@observer@*/
char *
SH_Attr_raw_get_value (const struct SH_Attr * attr)
	/*@*/
{
	return Attr_get_value (attr);
}

void
SH_Attr_raw_set_value (struct SH_Attr * attr,
                       /*@null@*/ /*@only@*/ char * value)
	/*@modifies attr->value@*/
{
	Attr_set_value (attr, value);
	return;
}

bool
SH_Attr_has_value (const struct SH_Attr * attr)
	/*@*/
{
	return Attr_has_value (attr);
}

bool
SH_Attr_is_equal (const struct SH_Attr * attr1,
                  const struct SH_Attr * attr2)
	/*@*/
{
	return Attr_is_equal (attr1, attr2);
}

bool
SH_Attr_is_equal_name (const struct SH_Attr * attr1,
                       const struct SH_Attr * attr2)
	/*@*/
{
	return Attr_is_equal_name (attr1, attr2);
}
