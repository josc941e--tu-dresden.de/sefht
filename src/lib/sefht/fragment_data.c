/*
 * fragment_data.c
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef SEFHT_FRAGMENT_DATA_C
#define SEFHT_FRAGMENT_DATA_C

#if !defined (SEFHT_SEFHT_H_INSIDE) && !defined (SEFHT_COMPILATION)
#error "Only <sefht/sefht.h> can be included directly."
#endif

#include "status.h"

#include "data.h"
#include "text.h"


#ifdef SEFHT_COMPILATION

enum SH_FRAGMENT_TYPE
{
	NODE,
	TEXT
};

struct SH_Fragment;

struct fragment_methods
{
	/*@null@*/
	/*@only@*/
	struct SH_Fragment *
	(*copy) (const struct SH_Fragment *,
	         /*@out@*/ /*@null@*/ struct SH_Status *)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

	void
	(*free) (/*@only@*/ struct SH_Fragment *)
	/*@modifies fragment@*/
	/*@releases fragment@*/;

	/*@null@*/
	/*@only@*/
	struct SH_Text *
	(*to_html) (const struct SH_Fragment *,
	            enum HTML_MODE,
	            unsigned int,
	            unsigned int,
	            const char *,
	            /*@out@*/ /*@null@*/ struct SH_Status *)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;
};

struct SH_Fragment
{
	enum SH_FRAGMENT_TYPE type;
	/*@shared@*/ const struct fragment_methods * methods;

	/*@dependent@*/ SH_Data * data;

	/*@null@*/ /*@dependent@*/ struct SH_NodeFragment * parent;
};

#endif /* SEFHT_COMPILATION */
#endif /* SEFHT_FRAGMENT_DATA_C */
