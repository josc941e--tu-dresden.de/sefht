/*
 * validator.c
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <stdlib.h>

#include "log.h"
#include "status.h"

#include "validator.h"


/* "validator_tag.c" is included twice,
 * because TAG_DATA must be defined,
 * before SH_VAlidator can be defined,
 * but SH_Validator must be defined,
 * before the functions in "validator_tag.c"
 * can use the definition, which themselves
 * are needed before the functions in this
 * file can be defined. */
#include "validator_tag.c"
struct SH_Validator
{
	TAG_DATA
};

#define VALIDATOR_IS_DEFINED
#include "validator_tag.c"


/*@null@*/
/*@only@*/
struct SH_Validator *
SH_Validator_new (/*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	struct SH_Validator * validator;
	validator = malloc (sizeof (struct SH_Validator));

	if (validator == NULL)
	{
		set_status (status, E_ALLOC, 4,
		            "Memory allocation for "
		            "SH_Validator failed.\n");

		return NULL;
	}

	if (!init_tags (validator, status))
	{
/* dangerous call to silence splint, should never be executed. */
#ifdef S_SPLINT_S
		free (validator->tags);
#endif
		free (validator);
		return NULL;
	}

	set_success (status);

	return validator;
}

/*@null@*/
/*@only@*/
struct SH_Validator *
SH_Validator_copy (const struct SH_Validator * validator,
                   /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/
{
	struct SH_Validator * copy;
	copy = malloc (sizeof (struct SH_Validator));

	if (copy == NULL)
	{
		set_status (status, E_ALLOC, 4,
		            "Memory allocation for "
		            "SH_Validator failed.\n");

		return NULL;
	}

	if (!copy_tags (copy, validator, status))
	{
/* dangerous call to silence splint, should never be executed. */
#ifdef S_SPLINT_S
		free (copy->tags);
#endif
		free (copy);
		return NULL;
	}

	set_success (status);

	return copy;
}

void
SH_Validator_free (/*@only@*/ struct SH_Validator * validator)
	/*@modifies validator@*/
	/*@releases validator@*/
{
	free_tags (validator);
	free (validator);

	return;
}
