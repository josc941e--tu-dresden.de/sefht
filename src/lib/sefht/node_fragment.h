/*
 * node_fragment.h
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef SEFHT_NODE_FRAGMENT_H
#define SEFHT_NODE_FRAGMENT_H

#if !defined (SEFHT_SEFHT_H_INSIDE) && !defined (SEFHT_COMPILATION)
#status "Only <sefht/sefht.h> can be included directly."
#endif

#include "status.h"

#include "attr.h"
#include "data.h"
#include "text.h"

#include "fragment.h"


#define INDENT_TEXT "\t"

typedef /*@abstract@*/ struct SH_NodeFragment SH_NodeFragment;


/*@null@*/
/*@only@*/
SH_Fragment /*@alt SH_NodeFragment@*/ *
SH_NodeFragment_new (const char * tag,
                     /*@dependent@*/ SH_Data * data,
                     /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

void
SH_NodeFragment_free (/*@only@*/ SH_NodeFragment * fragment)
	/*@modifies fragment@*/
	/*@releases fragment@*/;

/*@null@*/
/*@only@*/
SH_Fragment /*@alt SH_NodeFragment@*/ *
SH_NodeFragment_copy (const SH_NodeFragment * fragment,
                      /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

/*@null@*/
/*@only@*/
SH_Fragment /*@alt SH_NodeFragment@*/ *
SH_NodeFragment_deepcopy (const SH_NodeFragment * fragment,
                          /*@out@*/ /*@null@*/
                          struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_Fragment_is_NodeFragment (const SH_Fragment * fragment)
	/*@*/;

/*@null@*/
/*@dependent@*/
SH_NodeFragment *
SH_Fragment_get_parent (const SH_Fragment * fragment)
	/*@*/;

size_t
SH_NodeFragment_count_attrs (const SH_NodeFragment * fragment)
	/*@*/;

/*@null@*/
/*@observer@*/
const SH_Attr *
SH_NodeFragment_get_attr (const SH_NodeFragment * fragment,
                          size_t index,
                          /*@out@*/ /*@null@*/
                          struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_NodeFragment_insert_attr (SH_NodeFragment * fragment,
                             /*@only@*/ SH_Attr * attr,
                             size_t position,
                             /*@out@*/ /*@null@*/
                             struct SH_Status * status)
	/*@modifies fragment@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_NodeFragment_insert_attr_new (SH_NodeFragment * fragment,
                                 const char * name,
                                 /*@null@*/ const char * value,
                                 size_t position,
                                 /*@out@*/ /*@null@*/
                                 struct SH_Status * status)
	/*@modifies fragment@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_NodeFragment_insert_attr_raw_new (SH_NodeFragment * fragment,
                                     /*@only@*/ char * name,
                                     /*@null@*/ /*@only@*/ char * value,
                                     size_t position,
                                     /*@out@*/ /*@null@*/
                                     struct SH_Status * status)
	/*@modifies fragment@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_NodeFragment_insert_attr_copy (SH_NodeFragment * fragment,
                                  const SH_Attr * attr,
                                  size_t position,
                                  /*@out@*/ /*@null@*/
                                  struct SH_Status * status)
	/*@modifies fragment@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_NodeFragment_prepend_attr (SH_NodeFragment * fragment,
                              /*@only@*/ SH_Attr * attr,
                              /*@out@*/ /*@null@*/
                              struct SH_Status * status)
	/*@modifies fragment@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_NodeFragment_prepend_attr_new (SH_NodeFragment * fragment,
                                  const char * name,
                                  /*@null@*/ const char * value,
                                  /*@out@*/ /*@null@*/
                                  struct SH_Status * status)
	/*@modifies fragment@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_NodeFragment_prepend_attr_raw_new (SH_NodeFragment * fragment,
                                      /*@only@*/ char * name,
                                      /*@null@*/ /*@only@*/ char * value,
                                      /*@out@*/ /*@null@*/
                                      struct SH_Status * status)
	/*@modifies fragment@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_NodeFragment_prepend_attr_copy (SH_NodeFragment * fragment,
                                   const SH_Attr * attr,
                                   /*@out@*/ /*@null@*/
                                   struct SH_Status * status)
	/*@modifies fragment@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_NodeFragment_append_attr (SH_NodeFragment * fragment,
                             /*@only@*/ SH_Attr * attr,
                             /*@out@*/ /*@null@*/
                             struct SH_Status * status)
	/*@modifies fragment@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_NodeFragment_append_attr_new (SH_NodeFragment * fragment,
                                 const char * name,
                                 /*@null@*/ const char * value,
                                 /*@out@*/ /*@null@*/
                                 struct SH_Status * status)
	/*@modifies fragment@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_NodeFragment_append_attr_raw_new (SH_NodeFragment * fragment,
                                     /*@only@*/ char * name,
                                     /*@null@*/ /*@only@*/ char * value,
                                     /*@out@*/ /*@null@*/
                                     struct SH_Status * status)
	/*@modifies fragment@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;
bool
SH_NodeFragment_append_attr_copy (SH_NodeFragment * fragment,
                                  const SH_Attr * attr,
                                  /*@out@*/ /*@null@*/
                                  struct SH_Status * status)
	/*@modifies fragment@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_NodeFragment_remove_attr (SH_NodeFragment * fragment,
                             size_t position,
                             /*@out@*/ /*@null@*/
                             struct SH_Status * status)
	/*@modifies fragment@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

/*@null@*/
/*@only@*/
SH_Attr *
SH_NodeFragment_pop_attr (SH_NodeFragment * fragment,
                          size_t position,
                          /*@out@*/ /*@null@*/
                          struct SH_Status * status)
	/*@modifies fragment@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

/*@null@*/
/*@only@*/
char *
SH_NodeFragment_get_tag (const SH_NodeFragment * fragment,
                         /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

/*@null@*/
/*@observer@*/
const SH_Fragment *
SH_NodeFragment_get_child (const SH_NodeFragment * fragment,
                           size_t index,
                           /*@out@*/ /*@null@*/
                           struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_NodeFragment_is_child (const SH_NodeFragment * fragment,
                          const SH_Fragment * child)
	/*@*/;

bool
SH_NodeFragment_is_descendant (const SH_NodeFragment * fragment,
                          const SH_Fragment * child)
	/*@*/;

bool
SH_NodeFragment_is_parent (const SH_Fragment * fragment,
                           const SH_NodeFragment * parent)
	/*@*/;

bool
SH_NodeFragment_is_ancestor (const SH_Fragment * fragment,
                             const SH_NodeFragment * ancestor)
	/*@*/;

bool
SH_NodeFragment_insert_child (SH_NodeFragment * fragment,
                              /*@only@*/ SH_Fragment * child,
                              size_t position,
                              /*@out@*/ /*@null@*/
                              struct SH_Status * status)
	/*@modifies fragment@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_NodeFragment_prepend_child (SH_NodeFragment * fragment,
                              /*@only@*/ SH_Fragment * child,
                              /*@out@*/ /*@null@*/
                              struct SH_Status * status)
	/*@modifies fragment@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_NodeFragment_append_child (SH_NodeFragment * fragment,
                              /*@only@*/ SH_Fragment * child,
                              /*@out@*/ /*@null@*/
                              struct SH_Status * status)
	/*@modifies fragment@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_NodeFragment_insert_child_before (SH_Fragment * fragment,
                                     /*@only@*/ SH_Fragment * child,
                                     /*@out@*/ /*@null@*/
                                     struct SH_Status * status)
	/*@modifies fragment@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_NodeFragment_insert_child_after (SH_Fragment * fragment,
                                    /*@only@*/ SH_Fragment * child,
                                    /*@out@*/ /*@null@*/
                                    struct SH_Status * status)
	/*@modifies fragment@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_Fragment_remove (SH_Fragment * fragment,
                    /*@out@*/ /*@null@*/ struct SH_Status * status);

bool
SH_Fragment_delete (/*@only@*/ SH_Fragment * fragment,
                    /*@out@*/ /*@null@*/ struct SH_Status * status);

bool
SH_NodeFragment_remove_child (SH_NodeFragment * fragment,
                              size_t position,
                              /*@out@*/ /*@null@*/
                              struct SH_Status * status);

bool
SH_NodeFragment_delete_child (SH_NodeFragment * fragment,
                              size_t position,
                              /*@out@*/ /*@null@*/
                              struct SH_Status * status);

/*@null@*/
/*@only@*/
SH_Fragment *
SH_NodeFragment_pop_child (SH_NodeFragment * fragment,
                           size_t position,
                           /*@out@*/ /*@null@*/
                           struct SH_Status * status);

/*@null@*/
/*@only@*/
SH_Text *
SH_NodeFragment_to_html (const SH_NodeFragment * fragment,
                         enum HTML_MODE mode,
                         unsigned int indent_base,
                         unsigned int indent_step,
                         const char * indent_char,
                         /*@out@*/ /*@null@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

#endif /* SEFHT_NODE_FRAGMENT_H */
