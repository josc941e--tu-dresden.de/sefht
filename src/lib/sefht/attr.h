/*
 * attr.h
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef SEFHT_ATTR_H
#define SEFHT_ATTR_H

#if !defined (SEFHT_SEFHT_H_INSIDE) && !defined (SEFHT_COMPILATION)
#error "Only <sefht/sefht.h> can be included directly."
#endif

#include "status.h"

#include "data.h"


typedef /*@abstract@*/ struct SH_Attr SH_Attr;


/*@null@*/
/*@only@*/
struct SH_Attr *
SH_Attr_new (const char * name,
             /*@null@*/ const char * value,
             /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

/*@null@*/
/*@only@*/
struct SH_Attr *
SH_Attr_raw_new (/*@only@*/ char * name,
                 /*@null@*/ /*@only@*/ char * value,
                 /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

/*@null@*/
/*@only@*/
struct SH_Attr *
SH_Attr_copy (const struct SH_Attr * attr,
             /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

void
SH_Attr_free (/*@only@*/ SH_Attr * attr)
	/*@modifies attr@*/
	/*@releases attr@*/;

/*@null@*/
/*@only@*/
char *
SH_Attr_get_name (const SH_Attr * attr,
                  /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_Attr_set_name (SH_Attr * attr,
                  const char * name,
                  /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@modifies attr@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

/*@observer@*/
const char *
SH_Attr_raw_get_name (const SH_Attr * attr)
	/*@*/;

void
SH_Attr_raw_set_name (SH_Attr * attr, /*@only@*/ char * name)
	/*@modifies attr@*/;

/*@null@*/
/*@only@*/
char *
SH_Attr_get_value (const SH_Attr * attr,
                   /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

bool
SH_Attr_set_value (SH_Attr * attr,
                   /*@null@*/ const char * value,
                   /*@null@*/ /*@out@*/ struct SH_Status * status)
	/*@modifies attr@*/
	/*@globals fileSystem@*/
	/*@modifies fileSystem@*/
	/*@modifies status@*/;

/*@null@*/
/*@observer@*/
char *
SH_Attr_raw_get_value (const SH_Attr * attr)
	/*@*/;

void
SH_Attr_raw_set_value (SH_Attr * attr,
                       /*@null@*/ /*@only@*/ char * value)
	/*@modifies attr@*/;

bool
SH_Attr_has_value (const SH_Attr * attr)
	/*@*/;

bool
SH_Attr_is_equal (const SH_Attr * attr1, const SH_Attr * attr2)
	/*@*/;

bool
SH_Attr_is_equal_name (const SH_Attr * attr1, const SH_Attr * attr2)
	/*@*/;

#endif /* SEFHT_ATTR_H */
