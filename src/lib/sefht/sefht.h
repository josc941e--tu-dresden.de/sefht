/*
 * sefht.h
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#ifndef SEFHT_SEFHT_H
#define SEFHT_SEFHT_H

#define SEFHT_SEFHT_H_INSIDE

#include "log.h"
#include "macro.h"
#include "status.h"

#include "cms.h"
#include "data.h"
#include "fragment.h"
#include "node_fragment.h"
#include "text.h"
#include "validator.h"

#undef SEFHT_SEFHT_H_INSIDE

#endif /* SEFHT_SEFHT_H */
