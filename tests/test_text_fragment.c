/*
 * test_node_fragment.c
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <check.h>
#include <stdbool.h>
#include <stdlib.h>

#include "macro.h"
#include "status.h"

#include "text_fragment.c"


START_TEST(test_text_fragment_no_status)
{
	struct SH_TextFragment * fragment;
	SH_Data * data;

	/* setup */
	data = SH_Data_new (NULL);
	ck_assert_ptr_ne (NULL, data);

	/* test */
	fragment = (struct SH_TextFragment *)
	           SH_TextFragment_new (data, NULL);
	ck_assert_ptr_ne (NULL, fragment);
	ck_assert_ptr_ne (NULL, fragment->text);

	SH_TextFragment_free (fragment);

	/* cleanup */
	SH_Data_free (data);
}
END_TEST

START_TEST(test_text_fragment_with_status)
{
	struct SH_Status status;
	struct SH_TextFragment * fragment;
	SH_Data * data;

	/* setup */
	data = SH_Data_new (NULL);
	ck_assert_ptr_ne (NULL, data);

	/* test */
	_status_preinit (status);
	fragment = (struct SH_TextFragment *)
	           SH_TextFragment_new (data, &status);
	ck_assert_ptr_ne (NULL, fragment);
	ck_assert_ptr_ne (NULL, fragment->text);
	ck_assert_int_eq (SUCCESS, status.status);

	SH_TextFragment_free (fragment);

	/* cleanup */
	SH_Data_free (data);
}
END_TEST

START_TEST(test_text_fragment_copy_no_status)
{
	struct SH_TextFragment * fragment;
	struct SH_TextFragment * copy;
	SH_Data * data;

	/* setup */
	data = SH_Data_new (NULL);
	ck_assert_ptr_ne (NULL, data);

	fragment = (struct SH_TextFragment *)
	           SH_TextFragment_new (data, NULL);
	ck_assert_ptr_ne (NULL, fragment);

	/* test */
	copy = (struct SH_TextFragment *)
	       SH_TextFragment_copy (fragment, NULL);
	ck_assert_ptr_ne (NULL, copy);

	ck_assert_ptr_ne (fragment, copy);
	ck_assert_ptr_ne (fragment->text, copy->text);
	ck_assert_ptr_ne (NULL, copy->text);

	/* cleanup */
	SH_TextFragment_free (fragment);
	SH_TextFragment_free (copy);
	SH_Data_free (data);
}
END_TEST

START_TEST(test_text_fragment_copy_with_status)
{
	struct SH_Status status;
	struct SH_TextFragment * fragment;
	struct SH_TextFragment * copy;
	SH_Data * data;

	/* setup */
	data = SH_Data_new (NULL);
	ck_assert_ptr_ne (NULL, data);

	fragment = (struct SH_TextFragment *)
	           SH_TextFragment_new (data, NULL);
	ck_assert_ptr_ne (NULL, fragment);

	/* test */
	_status_preinit (status);
	copy = (struct SH_TextFragment *)
	       SH_TextFragment_copy (fragment, &status);
	ck_assert_ptr_ne (NULL, copy);
	ck_assert_int_eq (SUCCESS, status.status);

	ck_assert_ptr_ne (fragment, copy);
	ck_assert_ptr_ne (fragment->text, copy->text);
	ck_assert_ptr_ne (NULL, copy->text);

	/* cleanup */
	SH_TextFragment_free (fragment);
	SH_TextFragment_free (copy);
	SH_Data_free (data);
}
END_TEST

START_TEST(test_text_fragment_get_text)
{
	SH_Data * data;
	struct SH_TextFragment * fragment;
	const SH_Text * text;

	/* setup */
	data = SH_Data_new (NULL);
	ck_assert_ptr_ne (NULL, data);

	fragment = (SH_TextFragment *)SH_TextFragment_new (data, NULL);
	ck_assert_ptr_ne (NULL, fragment);

	/* test */
	text = SH_TextFragment_get_text (fragment);
	ck_assert_ptr_eq (text, fragment->text);

	/* cleanup */
	SH_TextFragment_free (fragment);
	SH_Data_free (data);
}
END_TEST

START_TEST(test_text_fragment_html_no_status)
{
	struct SH_TextFragment * fragment;
	SH_Data * data;
	struct SH_Text * text;

	/* setup */
	data = SH_Data_new (NULL);
	ck_assert_ptr_ne (NULL, data);

	fragment = (SH_TextFragment *)SH_TextFragment_new (data, NULL);
	ck_assert_ptr_ne (NULL, data);

	/* test */
	text = SH_TextFragment_to_html (fragment, WRAP,
	                                0, 1, "\t",
	                                NULL);
	ck_assert_ptr_ne (NULL, text);

	ck_assert_ptr_ne (text, fragment->text);

	/* cleanup */
	SH_Text_free (text);
	SH_TextFragment_free (fragment);
	SH_Data_free (data);
}
END_TEST

START_TEST(test_text_fragment_html_with_status)
{
	struct SH_Status status;
	struct SH_TextFragment * fragment;
	SH_Data * data;
	struct SH_Text * text;

	/* setup */
	data = SH_Data_new (NULL);
	ck_assert_ptr_ne (NULL, data);

	fragment = (SH_TextFragment *)SH_TextFragment_new (data, NULL);
	ck_assert_ptr_ne (NULL, data);

	/* test */
	_status_preinit (status);
	text = SH_TextFragment_to_html (fragment, WRAP,
	                                0, 1, "\t",
	                                &status);
	ck_assert_ptr_ne (NULL, text);
	ck_assert_int_eq (SUCCESS, status.status);

	ck_assert_ptr_ne (text, fragment->text);

	/* cleanup */
	SH_Text_free (text);
	SH_TextFragment_free (fragment);
	SH_Data_free (data);
}
END_TEST

Suite * test_suite (void)
{
	Suite *s;
	TCase *tc_core;

	s = suite_create ("Testsuite SeFHT TextFragment");

	/* Core test case */
	tc_core = tcase_create ("Core");

	tcase_add_test (tc_core, test_text_fragment_no_status);
	tcase_add_test (tc_core, test_text_fragment_with_status);
	tcase_add_test (tc_core, test_text_fragment_copy_no_status);
	tcase_add_test (tc_core, test_text_fragment_copy_with_status);
	tcase_add_test (tc_core, test_text_fragment_get_text);
	tcase_add_test (tc_core, test_text_fragment_html_no_status);
	tcase_add_test (tc_core, test_text_fragment_html_with_status);
	suite_add_tcase (s, tc_core);

	return s;
}

int main (void)
{
	int number_failed;
	Suite *s;
	SRunner *sr;

	s = test_suite ();
	sr = srunner_create (s);

	srunner_run_all (sr, CK_NORMAL);
	number_failed = srunner_ntests_failed (sr);
	srunner_free (sr);

	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
