/*
 * test_fragment.c
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <check.h>
#include <stdbool.h>
#include <stdlib.h>

#include "macro.h"
#include "status.h"

#include "node_fragment.h"
#include "text_fragment.h"

#include "fragment_class.c"


START_TEST(test_fragment_type)
{
	SH_Data * data;
	struct SH_Fragment * node_fragment;
	struct SH_Fragment * text_fragment;
	enum SH_FRAGMENT_TYPE type;

	/* setup */
	data = SH_Data_new (NULL);
	ck_assert_ptr_ne (NULL, data);

	node_fragment = SH_NodeFragment_new ("html", data, NULL);
	ck_assert_ptr_ne (NULL, node_fragment);

	text_fragment = SH_TextFragment_new (data, NULL);
	ck_assert_ptr_ne (NULL, text_fragment);

	/* test */
	type = get_type (node_fragment);
	ck_assert_int_eq (NODE, type);

	type = get_type (text_fragment);
	ck_assert_int_eq (TEXT, type);

	/* cleanup */
	SH_Fragment_free (node_fragment);
	SH_Fragment_free (text_fragment);
	SH_Data_free (data);
}
END_TEST

START_TEST(test_fragment_html_inline_no_status)
{
	SH_NodeFragment * parent;
	SH_Fragment * child;
	SH_Fragment * child_text;
	SH_Data * data;
	SH_Text * text;
	char * string;
	size_t length;
	bool result;

	/* setup */
	data = SH_Data_new (NULL);
	ck_assert_ptr_ne (NULL, data);

	parent = (SH_NodeFragment *)SH_NodeFragment_new ("html", data,
	                                                 NULL);
	ck_assert_ptr_ne (NULL, parent);

	result = SH_NodeFragment_append_attr_new (parent,
	                                          "lang", "de",
	                                          NULL);
	ck_assert_int_eq (TRUE, result);

	child = SH_NodeFragment_new ("body", data, NULL);
	ck_assert_ptr_ne (NULL, child);

	result = SH_NodeFragment_append_attr_new ((SH_NodeFragment *)child,
	                                          "id", "body",
	                                          NULL);
	ck_assert_int_eq (TRUE, result);

	result = SH_NodeFragment_append_attr_new ((SH_NodeFragment *)child,
	                                          "attr", NULL, NULL);
	ck_assert_int_eq (TRUE, result);

	result = SH_NodeFragment_append_child (parent, child, NULL);
	ck_assert_int_eq (TRUE, result);

	child_text = SH_TextFragment_new (data, NULL);
	ck_assert_ptr_ne (NULL, child_text);

	result = SH_TextFragment_append_string ((SH_TextFragment *)child_text,
	                                        "Lorem ipsum ...", NULL);
	ck_assert_int_eq (TRUE, result);

	result = SH_NodeFragment_append_child ((SH_NodeFragment *)child,
	                                       child_text, NULL);
	ck_assert_int_eq (TRUE, result);

	/* test */
	text = SH_NodeFragment_to_html (parent, INLINE,
	                                0, 1, INDENT_TEXT,
	                                NULL);
	ck_assert_ptr_ne (NULL, text);

	string = SH_Text_get_string (text, 0, SIZE_MAX, &length, NULL);
	ck_assert_str_eq (string, "<html lang=\"de\">"
	                          "<body id=\"body\" attr>"
	                          "Lorem ipsum ..."
	                          "</body>"
	                          "</html>");

	/* cleanup */
	free (string);
	SH_Text_free (text);
	SH_NodeFragment_free (parent);
	SH_Data_free (data);
}
END_TEST

START_TEST(test_fragment_html_inline_with_status)
{
	struct SH_Status status;
	SH_NodeFragment * parent;
	SH_Fragment * child;
	SH_Fragment * child_text;
	SH_Data * data;
	SH_Text * text;
	char * string;
	size_t length;
	bool result;

	/* setup */
	data = SH_Data_new (NULL);
	ck_assert_ptr_ne (NULL, data);

	parent = (SH_NodeFragment *)SH_NodeFragment_new ("html", data,
	                                                 NULL);
	ck_assert_ptr_ne (NULL, parent);

	result = SH_NodeFragment_append_attr_new (parent,
	                                          "lang", "de",
	                                          NULL);
	ck_assert_int_eq (TRUE, result);

	child = SH_NodeFragment_new ("body", data, NULL);
	ck_assert_ptr_ne (NULL, child);

	result = SH_NodeFragment_append_attr_new ((SH_NodeFragment *)child,
	                                          "id", "body",
	                                          NULL);
	ck_assert_int_eq (TRUE, result);

	result = SH_NodeFragment_append_attr_new ((SH_NodeFragment *)child,
	                                          "attr", NULL, NULL);
	ck_assert_int_eq (TRUE, result);

	result = SH_NodeFragment_append_child (parent, child, NULL);
	ck_assert_int_eq (TRUE, result);

	child_text = SH_TextFragment_new (data, NULL);
	ck_assert_ptr_ne (NULL, child_text);

	result = SH_TextFragment_append_string ((SH_TextFragment *)child_text,
	                                        "Lorem ipsum ...", NULL);
	ck_assert_int_eq (TRUE, result);

	result = SH_NodeFragment_append_child ((SH_NodeFragment *)child,
	                                       child_text, NULL);
	ck_assert_int_eq (TRUE, result);

	/* test */
	_status_preinit (status);
	text = SH_NodeFragment_to_html (parent, INLINE,
	                                0, 1, INDENT_TEXT,
	                                &status);
	ck_assert_ptr_ne (NULL, text);
	ck_assert_int_eq (status.status, SUCCESS);

	string = SH_Text_get_string (text, 0, SIZE_MAX, &length, NULL);
	ck_assert_str_eq (string, "<html lang=\"de\">"
	                          "<body id=\"body\" attr>"
	                          "Lorem ipsum ..."
	                          "</body>"
	                          "</html>");

	/* cleanup */
	free (string);
	SH_Text_free (text);
	SH_NodeFragment_free (parent);
	SH_Data_free (data);
}
END_TEST

START_TEST(test_fragment_html_wrap_no_status)
{
	SH_NodeFragment * parent;
	SH_Fragment * child;
	SH_Fragment * child_text;
	SH_Data * data;
	SH_Text * text;
	char * string;
	size_t length;
	bool result;

	/* setup */
	data = SH_Data_new (NULL);
	ck_assert_ptr_ne (NULL, data);

	parent = (SH_NodeFragment *)SH_NodeFragment_new ("html", data,
	                                                 NULL);
	ck_assert_ptr_ne (NULL, parent);

	result = SH_NodeFragment_append_attr_new (parent,
	                                          "lang", "de",
	                                          NULL);
	ck_assert_int_eq (TRUE, result);

	child = SH_NodeFragment_new ("body", data, NULL);
	ck_assert_ptr_ne (NULL, child);

	result = SH_NodeFragment_append_attr_new ((SH_NodeFragment *)child,
	                                          "id", "body",
	                                          NULL);
	ck_assert_int_eq (TRUE, result);

	result = SH_NodeFragment_append_attr_new ((SH_NodeFragment *)child,
	                                          "attr", NULL, NULL);
	ck_assert_int_eq (TRUE, result);

	result = SH_NodeFragment_append_child (parent, child, NULL);
	ck_assert_int_eq (TRUE, result);

	child_text = SH_TextFragment_new (data, NULL);
	ck_assert_ptr_ne (NULL, child_text);

	result = SH_TextFragment_append_string ((SH_TextFragment *)child_text,
	                                        "Lorem ipsum ...\n", NULL);
	ck_assert_int_eq (TRUE, result);

	result = SH_NodeFragment_append_child ((SH_NodeFragment *)child,
	                                       child_text, NULL);
	ck_assert_int_eq (TRUE, result);

	/* test */
	text = SH_NodeFragment_to_html (parent, WRAP,
	                                0, 1, INDENT_TEXT,
	                                NULL);
	ck_assert_ptr_ne (NULL, text);

	string = SH_Text_get_string (text, 0, SIZE_MAX, &length, NULL);
	ck_assert_str_eq (string, "<html lang=\"de\">\n"
	                          "\t<body id=\"body\" attr>\n"
	                          "\t\tLorem ipsum ...\n"
	                          "\t</body>\n"
	                          "</html>\n");

	/* cleanup */
	free (string);
	SH_Text_free (text);
	SH_NodeFragment_free (parent);
	SH_Data_free (data);
}
END_TEST

START_TEST(test_fragment_html_wrap_with_status)
{
	struct SH_Status status;
	SH_NodeFragment * parent;
	SH_Fragment * child;
	SH_Fragment * child_text;
	SH_Data * data;
	SH_Text * text;
	char * string;
	size_t length;
	bool result;

	/* setup */
	data = SH_Data_new (NULL);
	ck_assert_ptr_ne (NULL, data);

	parent = (SH_NodeFragment *)SH_NodeFragment_new ("html", data,
	                                                 NULL);
	ck_assert_ptr_ne (NULL, parent);

	result = SH_NodeFragment_append_attr_new (parent,
	                                          "lang", "de",
	                                          NULL);
	ck_assert_int_eq (TRUE, result);

	child = SH_NodeFragment_new ("body", data, NULL);
	ck_assert_ptr_ne (NULL, child);

	result = SH_NodeFragment_append_attr_new ((SH_NodeFragment *)child,
	                                          "id", "body",
	                                          NULL);
	ck_assert_int_eq (TRUE, result);

	result = SH_NodeFragment_append_attr_new ((SH_NodeFragment *)child,
	                                          "attr", NULL, NULL);
	ck_assert_int_eq (TRUE, result);

	result = SH_NodeFragment_append_child (parent, child, NULL);
	ck_assert_int_eq (TRUE, result);

	child_text = SH_TextFragment_new (data, NULL);
	ck_assert_ptr_ne (NULL, child_text);

	result = SH_TextFragment_append_string ((SH_TextFragment *)child_text,
	                                        "Lorem ipsum ...\n", NULL);
	ck_assert_int_eq (TRUE, result);

	result = SH_NodeFragment_append_child ((SH_NodeFragment *)child,
	                                       child_text, NULL);
	ck_assert_int_eq (TRUE, result);

	/* test */
	_status_preinit (status);
	text = SH_NodeFragment_to_html (parent, WRAP,
	                                0, 1, INDENT_TEXT,
	                                &status);
	ck_assert_ptr_ne (NULL, text);
	ck_assert_int_eq (status.status, SUCCESS);

	string = SH_Text_get_string (text, 0, SIZE_MAX, &length, NULL);
	ck_assert_str_eq (string, "<html lang=\"de\">\n"
	                          "\t<body id=\"body\" attr>\n"
	                          "\t\tLorem ipsum ...\n"
	                          "\t</body>\n"
	                          "</html>\n");

	/* cleanup */
	free (string);
	SH_Text_free (text);
	SH_NodeFragment_free (parent);
	SH_Data_free (data);
}
END_TEST

Suite * test_suite (void)
{
	Suite *s;
	TCase *tc_core;

	s = suite_create ("Testsuite SeFHT Fragment");

	/* Core test case */
	tc_core = tcase_create ("Core");

	tcase_add_test (tc_core, test_fragment_type);
	tcase_add_test (tc_core, test_fragment_html_inline_no_status);
	tcase_add_test (tc_core, test_fragment_html_inline_with_status);
	tcase_add_test (tc_core, test_fragment_html_wrap_no_status);
	tcase_add_test (tc_core, test_fragment_html_wrap_with_status);
	suite_add_tcase (s, tc_core);

	return s;
}

int main (void)
{
	int number_failed;
	Suite *s;
	SRunner *sr;

	s = test_suite ();
	sr = srunner_create (s);

	srunner_run_all (sr, CK_NORMAL);
	number_failed = srunner_ntests_failed (sr);
	srunner_free (sr);

	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
