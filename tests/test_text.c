/*
 * test_text.c
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <check.h>
#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>

/* lower SIZE_MAX as we try to reach it */
#include <limits.h>
#undef SIZE_MAX
#define SIZE_MAX 30

#include "macro.h"
#include "status.h"

/* C file is needed, because we want to override CHUNK_SIZE */
#define CHUNK_SIZE 5
#include "text.c"


START_TEST(test_text_no_status)
{
	struct SH_Text * text;

	text = SH_Text_new (NULL);
	ck_assert_ptr_ne (NULL, text);

	ck_assert_int_eq (0, text->data->length);
	ck_assert_int_eq (CHUNK_SIZE, text->data->size);
	ck_assert_ptr_ne (NULL, text->data->text);
	ck_assert_ptr_eq (NULL, text->data->next);

	SH_Text_free (text);
}
END_TEST

START_TEST(test_text_with_status)
{
	struct SH_Status status;
	struct SH_Text * text;

	_status_preinit (status);
	text = SH_Text_new (&status);
	ck_assert_ptr_ne (NULL, text);
	ck_assert_int_eq (status.status, SUCCESS);

	ck_assert_int_eq (0, text->data->length);
	ck_assert_int_eq (CHUNK_SIZE, text->data->size);
	ck_assert_ptr_ne (NULL, text->data->text);
	ck_assert_ptr_eq (NULL, text->data->next);

	SH_Text_free (text);
}
END_TEST

START_TEST(test_text_string_no_status)
{
	struct SH_Text * text;

	text = SH_Text_new_from_string ("12345", NULL);
	ck_assert_ptr_ne (NULL, text);

	ck_assert_int_eq (text->data->length, 5);
	ck_assert_int_eq (text->data->size, 6);
	ck_assert_str_eq (text->data->text, "12345");
	ck_assert_ptr_eq (text->data->next, NULL);

	SH_Text_free (text);
}
END_TEST

START_TEST(test_text_string_with_status)
{
	struct SH_Status status;
	struct SH_Text * text;

	_status_preinit (status);
	text = SH_Text_new_from_string ("12345", &status);
	ck_assert_ptr_ne (NULL, text);
	ck_assert_int_eq (status.status, SUCCESS);

	ck_assert_int_eq (text->data->length, 5);
	ck_assert_int_eq (text->data->size, 6);
	ck_assert_str_eq (text->data->text, "12345");
	ck_assert_ptr_eq (text->data->next, NULL);

	SH_Text_free (text);
}
END_TEST

START_TEST (test_text_copy_no_status)
{
	struct SH_Text * text;
	struct SH_Text * copy;

	/* setup */
	text = SH_Text_new_from_string ("Text12345", NULL);
	ck_assert_ptr_ne (NULL, text);

	/* test */
	copy = SH_Text_copy (text, NULL);
	ck_assert_ptr_ne (copy, NULL);

	ck_assert_ptr_ne (copy, text);
	ck_assert_ptr_ne (copy->data->text, text->data->text);
	ck_assert_str_eq (copy->data->text, text->data->text);

	/* cleanup */
	SH_Text_free (copy);
	SH_Text_free (text);
}
END_TEST

START_TEST (test_text_copy_with_status)
{
	struct SH_Status status;
	struct SH_Text * text;
	struct SH_Text * copy;

	/* setup */
	text = SH_Text_new_from_string ("Text12345", NULL);
	ck_assert_ptr_ne (NULL, text);

	/* test */
	_status_preinit (status);
	copy = SH_Text_copy (text, &status);
	ck_assert_int_eq (status.status, SUCCESS);
	ck_assert_ptr_ne (copy, NULL);

	ck_assert_ptr_ne (copy, text);
	ck_assert_ptr_ne (copy->data->text, text->data->text);
	ck_assert_str_eq (copy->data->text, text->data->text);

	/* cleanup */
	SH_Text_free (copy);
	SH_Text_free (text);
}
END_TEST

START_TEST(test_text_get_length_no_status)
{
	struct SH_Text * text;
	size_t length;
	char index[2] = {1, 0};

	text = SH_Text_new (NULL);
	ck_assert_ptr_ne (NULL, text);

	/* length == 0 */
	length = SH_Text_get_length (text, NULL);
	ck_assert_int_eq (length, 0);

	for (; index[0] <= SIZE_MAX; index[0]++)
	{
		SH_Text_append_string (text, index, NULL);
	}

	/* length == SIZE_MAX */
	length = SH_Text_get_length (text, NULL);
	ck_assert_int_eq (length, SIZE_MAX);

	SH_Text_append_string (text, index, NULL);

	/* length > SIZE_MAX */
	length = SH_Text_get_length (text, NULL);
	ck_assert_int_eq (length, SIZE_MAX);

	SH_Text_free (text);
}
END_TEST

START_TEST(test_text_get_length_with_status)
{
	struct SH_Status status;
	struct SH_Text * text;
	size_t length;
	char index[2] = {1, 0};

	text = SH_Text_new (NULL);
	ck_assert_ptr_ne (NULL, text);

	/* length == 0 */
	_status_preinit (status);
	length = SH_Text_get_length (text, &status);
	ck_assert_int_eq (length, 0);
	ck_assert_int_eq (status.status, SUCCESS);


	for (; index[0] <= SIZE_MAX; index[0]++)
	{
		SH_Text_append_string (text, index, NULL);
	}


	/* length == SIZE_MAX */
	_status_preinit (status);
	length = SH_Text_get_length (text, &status);
	ck_assert_int_eq (length, SIZE_MAX);
	ck_assert_int_eq (status.status, SUCCESS);

	SH_Text_append_string (text, index, NULL);

	/* length > SIZE_MAX */
	_status_preinit (status);
	length = SH_Text_get_length (text, &status);
	ck_assert_int_eq (length, SIZE_MAX);
	ck_assert_int_eq (status.status, E_DOMAIN);

	SH_Text_free (text);
}
END_TEST

START_TEST(test_text_get_char_no_status)
{
	struct SH_Text * text;
	unsigned int i;
	char * c;

	/* setup */
	text = SH_Text_new_from_string ("abcd", NULL);
	SH_Text_append_string (text, "efgh", NULL);
	SH_Text_append_string (text, "ijkl", NULL);

	/* success */
	for (i = 0; i < 12; i++)
	{
		c = SH_Text_get_char (text, i, NULL);
		ck_assert_ptr_ne (c, NULL);
		ck_assert_int_eq (*c, 97 + i);
		free (c);
	}

	/* wrong index */
	c = SH_Text_get_char (text, 13, NULL);
	ck_assert_ptr_eq (c, NULL);

	/* cleanup */
	SH_Text_free (text);
}
END_TEST

START_TEST(test_text_get_char_with_status)
{
	struct SH_Status status;
	struct SH_Text * text;
	unsigned int i;
	char * c;

	/* setup */
	text = SH_Text_new_from_string ("abcd", NULL);
	SH_Text_append_string (text, "efgh", NULL);
	SH_Text_append_string (text, "ijkl", NULL);

	/* success */
	for (i = 0; i < 12; i++)
	{
		_status_preinit (status);
		c = SH_Text_get_char (text, i, &status);
		ck_assert_ptr_ne (c, NULL);
		ck_assert_int_eq (status.status, SUCCESS);

		ck_assert_int_eq (*c, 97 + i);
		free (c);
	}

	/* wrong index */
	_status_preinit (status);
	c = SH_Text_get_char (text, 13, &status);
	ck_assert_ptr_eq (c, NULL);
	ck_assert_int_eq (status.status, E_VALUE);

	/* cleanup */
	SH_Text_free (text);
}
END_TEST

START_TEST(test_text_get_string_no_status)
{
	struct SH_Text * text;
	char * result;
	size_t length = -1;
	char index[2] = {1, 0};

	/* setup */
	text = SH_Text_new (NULL);
	for (; index[0] < 100; index[0]++)
	{
		SH_Text_append_string (text, index, NULL);
	}

	/* single segment */
	result = SH_Text_get_string (text, 59, 8, &length, NULL);
	ck_assert_str_eq (result, "<=>?@ABC");
	ck_assert_int_eq (length, 8);
	free (result);

	/* multiple segment */
	result = SH_Text_get_string (text, 47, 43, &length, NULL);
	ck_assert_str_eq (result, "0123456789:;<=>?@"
	                          "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	ck_assert_int_eq (length, 43);
	free (result);

	/* pass over end */
	result = SH_Text_get_string (text, 96, 10, &length, NULL);
	ck_assert_str_eq (result, "abc");
	ck_assert_int_eq (length, 3);
	free (result);

	/* out of range */
	result = SH_Text_get_string (text, 99, 0, &length, NULL);
	ck_assert_ptr_eq (result, NULL);
	ck_assert_int_eq (length, 0);

	/* cleanup */
	SH_Text_free (text);
}
END_TEST

START_TEST(test_text_get_string_with_status)
{
	struct SH_Status status;
	struct SH_Text * text;
	char * result;
	size_t length = -1;
	char index[2] = {1, 0};

	/* setup */
	text = SH_Text_new (NULL);
	for (; index[0] < 100; index[0]++)
	{
		SH_Text_append_string (text, index, NULL);
	}

	/* single segment */
	_status_preinit (status);
	result = SH_Text_get_string (text, 59, 8, &length, &status);
	ck_assert_str_eq (result, "<=>?@ABC");
	ck_assert_int_eq (length, 8);
	ck_assert_int_eq (status.status, SUCCESS);
	free (result);

	/* multiple segment */
	_status_preinit (status);
	result = SH_Text_get_string (text, 47, 43, &length, &status);
	ck_assert_str_eq (result, "0123456789:;<=>?@"
	                          "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	ck_assert_int_eq (length, 43);
	ck_assert_int_eq (status.status, SUCCESS);
	free (result);

	/* pass over end */
	_status_preinit (status);
	result = SH_Text_get_string (text, 96, 10, &length, &status);
	ck_assert_str_eq (result, "abc");
	ck_assert_int_eq (length, 3);
	ck_assert_int_eq (status.status, SUCCESS);
	free (result);

	/* out of range */
	_status_preinit (status);
	result = SH_Text_get_string (text, 99, 0, &length, &status);
	ck_assert_ptr_eq (result, NULL);
	ck_assert_int_eq (length, 0);
	ck_assert_int_eq (status.status, E_VALUE);

	/* cleanup */
	SH_Text_free (text);
}
END_TEST

START_TEST(test_text_get_range_no_status)
{
	struct SH_Text * text;
	char * result;
	size_t length = -1;
	char index[2] = {1, 0};

	/* setup */
	text = SH_Text_new (NULL);
	for (; index[0] < 100; index[0]++)
	{
		SH_Text_append_string (text, index, NULL);
	}

	/* single segment */
	result = SH_Text_get_range (text, 59, 67, &length, NULL);
	ck_assert_str_eq (result, "<=>?@ABC");
	ck_assert_int_eq (length, 8);
	free (result);

	/* multiple segment */
	result = SH_Text_get_range (text, 47, 90, &length, NULL);
	ck_assert_str_eq (result, "0123456789:;<=>?@"
	                          "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	ck_assert_int_eq (length, 43);
	free (result);

	/* pass over end */
	result = SH_Text_get_range (text, 96, 106, &length, NULL);
	ck_assert_str_eq (result, "abc");
	ck_assert_int_eq (length, 3);
	free (result);

	/* out of range */
	result = SH_Text_get_range (text, 99, 99, &length, NULL);
	ck_assert_ptr_eq (result, NULL);
	ck_assert_int_eq (length, 0);

	/* cleanup */
	SH_Text_free (text);
}
END_TEST

START_TEST(test_text_get_range_with_status)
{
	struct SH_Status status;
	struct SH_Text * text;
	char * result;
	size_t length = -1;
	char index[2] = {1, 0};

	/* setup */
	text = SH_Text_new (NULL);
	for (; index[0] < 100; index[0]++)
	{
		SH_Text_append_string (text, index, NULL);
	}

	/* single segment */
	_status_preinit (status);
	result = SH_Text_get_range (text, 59, 67, &length, &status);
	ck_assert_str_eq (result, "<=>?@ABC");
	ck_assert_int_eq (length, 8);
	ck_assert_int_eq (status.status, SUCCESS);
	free (result);

	/* multiple segment */
	_status_preinit (status);
	result = SH_Text_get_range (text, 47, 90, &length, &status);
	ck_assert_str_eq (result, "0123456789:;<=>?@"
	                          "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	ck_assert_int_eq (length, 43);
	ck_assert_int_eq (status.status, SUCCESS);
	free (result);

	/* pass over end */
	_status_preinit (status);
	result = SH_Text_get_range (text, 96, 106, &length, &status);
	ck_assert_str_eq (result, "abc");
	ck_assert_int_eq (length, 3);
	ck_assert_int_eq (status.status, SUCCESS);
	free (result);

	/* out of range */
	_status_preinit (status);
	result = SH_Text_get_range (text, 99, 99, &length, &status);
	ck_assert_ptr_eq (result, NULL);
	ck_assert_int_eq (length, 0);
	ck_assert_int_eq (status.status, E_VALUE);

	/* cleanup */
	SH_Text_free (text);
}
END_TEST

START_TEST(test_text_set_char_no_status)
{
	struct SH_Text * text;
	char * string;
	bool result;

	/* setup */
	text = SH_Text_new (NULL);
	ck_assert_ptr_ne (NULL, text);

	result = SH_Text_append_string (text,
	                                "abcdefghijklmnopqrstuvwxyz",
	                                NULL);
	ck_assert_int_eq (TRUE, result);

	result = SH_Text_append_text (text, text, NULL);
	ck_assert_int_eq (TRUE, result);

	/* first segment */
	result = SH_Text_set_char (text, 20, 35, NULL);
	ck_assert_int_eq (result, TRUE);

	/* second segment */
	result = SH_Text_set_char (text, 40, 35, NULL);
	ck_assert_int_eq (result, TRUE);

	/* index error */
	result = SH_Text_set_char (text, 60, 35, NULL);
	ck_assert_int_eq (result, FALSE);

	string = SH_Text_get_string (text, 0, 52, NULL, NULL);
	ck_assert_ptr_ne (NULL, string);
	ck_assert_str_eq (string, "abcdefghijklmnopqrst#vwxyz"
	                          "abcdefghijklmn#pqrstuvwxyz");

	/* cleanup */
	free (string);

	SH_Text_free (text);
}
END_TEST

START_TEST(test_text_set_char_with_status)
{
	struct SH_Status status;
	struct SH_Text * text;
	char * string;
	bool result;

	/* setup */
	text = SH_Text_new (NULL);
	ck_assert_ptr_ne (NULL, text);

	result = SH_Text_append_string (text,
	                                "abcdefghijklmnopqrstuvwxyz",
	                                NULL);
	ck_assert_int_eq (TRUE, result);

	result = SH_Text_append_text (text, text, NULL);
	ck_assert_int_eq (TRUE, result);

	/* first segment */
	_status_preinit (status);
	result = SH_Text_set_char (text, 20, 35, &status);
	ck_assert_int_eq (result, TRUE);
	ck_assert_int_eq (status.status, SUCCESS);

	/* second segment */
	_status_preinit (status);
	result = SH_Text_set_char (text, 40, 35, &status);
	ck_assert_int_eq (result, TRUE);
	ck_assert_int_eq (status.status, SUCCESS);

	/* index error */
	_status_preinit (status);
	result = SH_Text_set_char (text, 60, 35, &status);
	ck_assert_int_eq (result, FALSE);
	ck_assert_int_eq (status.status, E_VALUE);

	string = SH_Text_get_string (text, 0, 52, NULL, NULL);
	ck_assert_ptr_ne (NULL, string);
	ck_assert_str_eq (string, "abcdefghijklmnopqrst#vwxyz"
	                          "abcdefghijklmn#pqrstuvwxyz");

	/* cleanup */
	free (string);

	SH_Text_free (text);
}
END_TEST

START_TEST (test_text_append)
{
	struct SH_Status status;
	struct SH_Text * text1;
	struct SH_Text * text2;
	bool boolean;
	char * string;
	size_t length;

	text1 = SH_Text_new (NULL);
	text2 = SH_Text_new (NULL);

	/* append_string - without error */
	boolean = SH_Text_append_string (text1, "Text1", NULL);
	ck_assert_int_eq (boolean, TRUE);
	string = SH_Text_get_string (text1, 0, SIZE_MAX, &length, NULL);
	ck_assert_str_eq (string, "Text1");
	ck_assert_int_eq (length, 5);
	free (string);

	/* append_string - with error */
	_status_preinit (status);
	boolean = SH_Text_append_string (text2, "Text2", &status);
	ck_assert_int_eq (boolean, TRUE);
	ck_assert_int_eq (status.status, SUCCESS);
	string = SH_Text_get_string (text2, 0, SIZE_MAX, &length, NULL);
	ck_assert_str_eq (string, "Text2");
	ck_assert_int_eq (length, 5);
	free (string);


	/* append_text - without error */
	boolean = SH_Text_append_text (text1, text2, NULL);
	ck_assert_int_eq (boolean, TRUE);
	string = SH_Text_get_string (text1, 0, SIZE_MAX, &length, NULL);
	ck_assert_str_eq (string, "Text1Text2");
	ck_assert_int_eq (length, 10);
	free (string);

	/* append_text - with error */
	_status_preinit (status);
	boolean = SH_Text_append_text (text1, text2, &status);
	ck_assert_int_eq (boolean, TRUE);
	ck_assert_int_eq (status.status, SUCCESS);
	string = SH_Text_get_string (text1, 0, SIZE_MAX, &length, NULL);
	ck_assert_str_eq (string, "Text1Text2Text2");
	ck_assert_int_eq (length, 15);
	free (string);


	/* join - no error */
	SH_Text_join (text1, text2);
	string = SH_Text_get_string (text1, 0, SIZE_MAX, &length, NULL);
	ck_assert_str_eq (string, "Text1Text2Text2Text2");
	ck_assert_int_eq (length, 20);
	free (string);


	SH_Text_free (text1);
}
END_TEST

START_TEST(test_text_print)
{
	struct SH_Text * text;

	text = SH_Text_new_from_string ("abcdefghijkklmnopqrstuvwxyz",
					NULL);
	SH_Text_append_text (text, text, NULL);

	SH_Text_print (text);

	SH_Text_free (text);
}
END_TEST

Suite * text_suite (void)
{
	Suite *s;
	TCase *tc_core;

	s = suite_create ("Testsuite SeFHT Text");

	/* Core test case */
	tc_core = tcase_create ("Core");

	tcase_add_test (tc_core, test_text_no_status);
	tcase_add_test (tc_core, test_text_with_status);
	tcase_add_test (tc_core, test_text_string_no_status);
	tcase_add_test (tc_core, test_text_string_with_status);
	tcase_add_test (tc_core, test_text_copy_no_status);
	tcase_add_test (tc_core, test_text_copy_with_status);
	tcase_add_test (tc_core, test_text_get_length_no_status);
	tcase_add_test (tc_core, test_text_get_length_with_status);
	tcase_add_test (tc_core, test_text_get_char_no_status);
	tcase_add_test (tc_core, test_text_get_char_with_status);
	tcase_add_test (tc_core, test_text_get_string_no_status);
	tcase_add_test (tc_core, test_text_get_string_with_status);
	tcase_add_test (tc_core, test_text_get_range_no_status);
	tcase_add_test (tc_core, test_text_get_range_with_status);
	tcase_add_test (tc_core, test_text_set_char_no_status);
	tcase_add_test (tc_core, test_text_set_char_with_status);
	tcase_add_test (tc_core, test_text_append);
	tcase_add_test (tc_core, test_text_print);
	suite_add_tcase (s, tc_core);

	return s;
}

int main (void)
{
	int number_failed;
	Suite *s;
	SRunner *sr;

	s = text_suite ();
	sr = srunner_create (s);

	srunner_run_all (sr, CK_NORMAL);
	number_failed = srunner_ntests_failed (sr);
	srunner_free (sr);

	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

