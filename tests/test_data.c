/*
 * test_data.c
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <check.h>
#include <stdlib.h>

#include "status.h"

#include "data.c"


START_TEST(test_data)
{
	struct SH_Status status;
	struct SH_Data * data;

	data = SH_Data_new (NULL);
	ck_assert_int_ne ((long int) data, (long int) NULL);

	ck_assert_int_eq (data->page_n, 0);
	ck_assert_int_eq (data->last_page, PAGE_ERR);

	SH_Data_free (data);

	_status_preinit (status);
	data = SH_Data_new (&status);
	ck_assert_int_ne ((long int) data, (long int) NULL);
	ck_assert_int_eq (status.status, SUCCESS);

	ck_assert_int_eq (data->page_n, 0);
	ck_assert_int_eq (data->last_page, PAGE_ERR);

	SH_Data_free (data);
}
END_TEST

START_TEST(test_data_register_page)
{
	struct SH_Status status;
	struct SH_Data * data;
	const char * page1 = "Homepage";
	const char * page2 = "Login";
	const char * page3 = "Register";
	const char * page4 = "Weather";
	const char * page5 = "Traffic";
	const char * page6 = "Impressum";
	page_t page;

	data = SH_Data_new (NULL);

	/* success without error */
	page = SH_Data_register_page (data, page1, NULL);
	ck_assert_int_eq (page, 1);

	ck_assert_int_eq (data->page_n, 1);
	ck_assert_int_eq (data->last_page, page);

	ck_assert_int_eq (data->pages[0].id, page);
	ck_assert_str_eq (data->pages[0].name, page1);

	/* fail without error */
	data->page_n = SIZE_MAX;

	page = SH_Data_register_page (data, page2, NULL);
	ck_assert_int_eq (page, PAGE_ERR);

	ck_assert_int_eq (data->page_n, SIZE_MAX);
	ck_assert_int_eq (data->last_page, 1);

	/* fail2 without error */
	data->page_n = 1;
	data->last_page = PAGE_MAX;

	page = SH_Data_register_page (data, page3, NULL);
	ck_assert_int_eq (page, PAGE_ERR);

	ck_assert_int_eq (data->page_n, 1);
	ck_assert_int_eq (data->last_page, PAGE_MAX);

	/* success with error */
	data->last_page = 1;

	_status_preinit (status);
	page = SH_Data_register_page (data, page4, &status);
	ck_assert_int_eq (page, 2);
	ck_assert_int_eq (status.status, SUCCESS);

	ck_assert_int_eq (data->page_n, 2);
	ck_assert_int_eq (data->last_page, page);

	ck_assert_int_eq (data->pages[1].id, page);
	ck_assert_str_eq (data->pages[1].name, page4);

	/* fail with error */
	data->page_n = SIZE_MAX;

	_status_preinit (status);
	page = SH_Data_register_page (data, page5, &status);
	ck_assert_int_eq (page, PAGE_ERR);
	ck_assert_int_eq (status.status, E_DOMAIN);

	ck_assert_int_eq (data->page_n, SIZE_MAX);
	ck_assert_int_eq (data->last_page, 2);

	/* fail2 with error */
	data->page_n = 2;
	data->last_page = PAGE_MAX;

	_status_preinit (status);
	page = SH_Data_register_page (data, page6, &status);
	ck_assert_int_eq (page, PAGE_ERR);
	ck_assert_int_eq (status.status, E_DOMAIN);

	ck_assert_int_eq (data->page_n, 2);
	ck_assert_int_eq (data->last_page, PAGE_MAX);

	SH_Data_free (data);
}
END_TEST

Suite * data_suite (void)
{
	Suite *s;
	TCase *tc_core;

	s = suite_create ("Testsuite SeFHT data");

	/* Core test case */
	tc_core = tcase_create ("Core");

	tcase_add_test (tc_core, test_data);
	tcase_add_test (tc_core, test_data_register_page);
	suite_add_tcase (s, tc_core);

	return s;
}

int main (void)
{
	int number_failed;
	Suite *s;
	SRunner *sr;

	s = data_suite ();
	sr = srunner_create (s);

	srunner_run_all (sr, CK_NORMAL);
	number_failed = srunner_ntests_failed (sr);
	srunner_free (sr);

	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

