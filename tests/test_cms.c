/*
 * test_cms.c
 *
 * Copyright 2023 Jonathan Schöbel <jonathan@xn--schbel-yxa.info>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <check.h>
#include <stdlib.h>

#include "status.h"

#include "cms.c"


START_TEST(test_cms)
{
	struct SH_Status status;
	struct SH_Cms * cms;

	cms = SH_Cms_new (NULL);
	ck_assert_int_ne ((long int) cms, (long int) NULL);

	SH_Cms_free (cms);

	_status_preinit (status);
	cms = SH_Cms_new (&status);
	ck_assert_int_ne ((long int) cms, (long int) NULL);
	ck_assert_int_eq (status.status, SUCCESS);

	SH_Cms_free (cms);
}
END_TEST

Suite * cms_suite (void)
{
	Suite *s;
	TCase *tc_core;

	s = suite_create ("Testsuite SeFHT CMS");

	/* Core test case */
	tc_core = tcase_create ("Core");

	tcase_add_test (tc_core, test_cms);
	suite_add_tcase (s, tc_core);

	return s;
}

int main (void)
{
	int number_failed;
	Suite *s;
	SRunner *sr;

	s = cms_suite ();
	sr = srunner_create (s);

	srunner_run_all (sr, CK_NORMAL);
	number_failed = srunner_ntests_failed (sr);
	srunner_free (sr);

	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

